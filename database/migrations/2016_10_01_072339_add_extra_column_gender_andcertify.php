<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraColumnGenderAndcertify extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('power_ups', function (Blueprint $table) {
            $table->string('gender', 250)->after('type');
            $table->integer('certify')->after('gender');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('power_ups', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('certify');
        });
    }
}
