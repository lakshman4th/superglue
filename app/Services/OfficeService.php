<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Office;

class OfficeService extends Service
{
	/**
	 * Create office for space
	 * @param  Request $request  
	 * @param  int  $space_id 
	 * @return null            
	 */
	public function create(Request $request)
	{
		$this->validate($request, [
			'name' => 'required',
			'length'=> 'required|numeric',
			'width'=> 'required|numeric',
			'capacity' => 'required|numeric',
			'signup_fee'=> 'required|numeric',
			'cost'=> 'required|numeric',
		]);

		$office = new Office;
		$office->space_id = $request->space_id;
		$office->name = $request->name;
		$office->features = $request->features;
		$office->length = $request->length;
		$office->width = $request->width;
		$office->capacity = $request->capacity;
		$office->signup_fee = $request->signup_fee;
		$office->cost = $request->cost;
		$office->save();

		return $office;

	}

	/**
	 * Edit office details
	 * @param  Request $request 
	 * @param  int  $office_id 
	 * @return object           
	 */
	public function edit(Request $request,$office_id)
	{
		$this->validate($request,[
				'name' => 'required',
				'length'=> 'required|numeric',
				'width'=> 'required|numeric',
				'capacity' => 'required|numeric',
				'signup_fee'=> 'required|numeric',
				'cost'=> 'required|numeric',
			]);

		$office = Office::find($office_id);
		$office->name = $request->name;
		$office->features = $request->get('features','');
		$office->length = $request->length;
		$office->width = $request->width;
		$office->capacity = $request->capacity;
		$office->signup_fee = $request->signup_fee;
		$office->cost = $request->cost;
		$office->save();

		return $office;
	}
}
