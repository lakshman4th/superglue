<?php

namespace App\Services;

use Illuminate\Http\Request;

use App\Payment;
use App\Services\InvoiceService;
use Auth;

class PaymentService extends Service
{
	/**
	 * Add payment details to invoice. Calculates for invoice status afterwards
	 * @param  Request $request    
	 * @return null              
	 */
	public function add(Request $request)
	{
		$this->validate($request,[
				'invoice_id' => 'required',
				'account_id' => 'required',
				'amount' => 'required|numeric',
				'method' => 'required',
				'payment_date' => 'required|date_format:Y-m-d',
			]);

		$payment = new Payment;
		$payment->account_id = $request->account_id;
		$payment->invoice_id = $request->invoice_id;
		$payment->stripe_transaction_id = null;
		$payment->amount = $request->amount;
		$payment->method = $request->method;
		$payment->payment_date = $request->payment_date;
		$payment->save();

		$invoiceService = new InvoiceService;
		$invoiceService->updateStatus($request->invoice_id);

		$this->addTimeline([
			'created_by' => Auth::user()->id,
			'account_id'	=> $request->account_id,
			'title'		 => 'Added payment',
			'message'    => Auth::user()->email . ' added payment for invoice id - '.$request->invoice_id.' for $'.number_format($request->amount),
			'type'       => 'info'
		]);
	}

	/**
	 * Edit payment details. Calculates for invoice status afterwards
	 * @param  Request $request    
	 * @param  int  $payment_id 
	 * @return null              
	 */
	public function edit(Request $request,$payment_id)
	{
		$this->validate($request,[
				'amount' => 'required|numeric',
				'method' => 'required',
				'payment_date' => 'required|date_format:Y-m-d',
			]);

		$payment = Payment::findOrFail($payment_id);
		$payment->amount = $request->amount;
		$payment->method = $request->method;
		$payment->payment_date = $request->payment_date;
		$payment->save();

		$invoiceService = new InvoiceService;
		$invoiceService->updateStatus($payment->invoice_id);
	}

	/**
	 * Deletes payment for invoice. Calculates for invoice status afterwards
	 * @param  int $payment_id 
	 * @return null             
	 */
	public function delete($payment_id)
	{
		$payment = Payment::findOrFail($payment_id);
		$payment->delete();

		$invoiceService = new InvoiceService;
		$invoiceService->updateStatus($payment->invoice_id);
	}
}
