<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Company;
use Carbon\Carbon;
use App\CompanyStats;

class CompanyService
{
	/**
	 * Store company details along with Company Stats
	 * @param  Request $request
	 * @return array
	 */
    public function storeCompany(Request $request)
    {
        $company = new Company;
        $company->name = $request->name;
        $company->account_id = $request->account_id;
        $company->space_id = $request->space_id;
        $company->industry = $request->name;
        $company->abn = $request->abn;
        $company->date_started = Carbon::createFromFormat('Y-m-d',$request->date_started)->format('Y-m-d');
        $company->save();

        $this->storeCompanyStats($request,$company->id);

        return $company;
    }

    /**
     * Store Company Stats
     * @param  Request $request
     * @param  int  $company_id
     * @return null
     */
    private function storeCompanyStats(Request $request,$company_id)
    {
        $companyStats = new CompanyStats;
        $companyStats->company_id = $company_id;
        $companyStats->employees = $request->employees;
        $companyStats->investment = $request->investment;
        $companyStats->revenue = $request->revenue;
        $companyStats->save();
    }
}
