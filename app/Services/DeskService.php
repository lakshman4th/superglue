<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Desk;

class DeskService extends Service
{
	/**
	 * Create desk for space
	 * @param  Request $request  
	 * @param  int  $space_id 
	 * @return null            
	 */
	public function create(Request $request)
	{
		$this->validate($request, [
			'name' => 'required',
			'signup_fee' => 'required',
			'cost'=> 'required|numeric',
		]);

		$desk = new Desk;
		$desk->space_id = $request->space_id;
		$desk->name = $request->name;
		$desk->signup_fee = $request->signup_fee;
		$desk->cost = $request->cost;
		$desk->save();

	}

	/**
	 * Edit desk details
	 * @param  Request $request 
	 * @param  int  $desk_id 
	 * @return object           
	 */
	public function edit(Request $request,$desk_id)
	{
		$this->validate($request,[
				'name'          => 'required',
				'signup_fee' => 'required',
				'cost'      => 'required|numeric',
			]);

		$desk = Desk::find($desk_id);
		$desk->name = $request->name;
		$desk->signup_fee = $request->signup_fee;
		$desk->cost = $request->cost;
		$desk->save();

		return $desk;
	}
}
