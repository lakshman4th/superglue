<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Room;

class RoomService extends Service
{
	/**
	 * Create room for space
	 * @param  Request $request
	 * @param  int  $space_id
	 * @return null
	 */
	public function create(Request $request,$space_id)
	{
		$this->validate($request,[
			'name'             => 'required',
			'capacity'         => 'required|numeric',
			'credits_per_hour' => 'required|numeric',
		]);

		$room = new Room;
		$room->space_id = $space_id;
		$room->name = $request->name;
		$room->description = $request->description;
		$room->capacity = $request->capacity;
		$room->credits_per_hour = $request->credits_per_hour;
		$room->save();
	}

	/**
	 * Edit room details
	 * @param  Request $request
	 * @param  int  $room_id
	 * @return object
	 */
	public function edit(Request $request,$room_id)
	{
		$this->validate($request,[
			'name'             => 'required',
			'capacity'         => 'required|numeric',
			'credits_per_hour' => 'required|numeric',
		]);

		$room = Room::find($room_id);
		$room->name = $request->name;
		$room->description = $request->description;
		$room->capacity = $request->capacity;
		$room->credits_per_hour = $request->credits_per_hour;
		$room->save();

		return $room;
	}
}
