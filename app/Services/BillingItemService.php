<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\BillingItem;
use App\Plan;

class BillingItemService extends Service
{
	public function add(Request $request,$account_id)
	{
		$this->validate($request,[
				'plan' => 'required',
				'start_date' => 'required|date_format:Y-m-d'
			]);

		$plan = Plan::find($request->plan);

		$item = new BillingItem;
		$item->account_id = $account_id;
		$item->plan_id = $plan->id;
		$item->name = 'Plan: '.$plan->name;
		$item->cost = $plan->cost;
		$item->num_credits = $plan->credit_per_renewal;
		$item->start_date = $request->start_date;
		$item->save();
	}
}
