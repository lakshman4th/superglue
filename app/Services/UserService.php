<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Invitation;
use App\Timeline;
use Auth;
use Mail;
use App\Account;
use App\Space;
use App\HighFive;
use Carbon\Carbon;
use App\Exceptions\ServiceValidationException;

class UserService extends Service
{
	/**
	 * Store invited user details
	 * @param  Request $request
	 * @return array
	 */
	public function invite(Request $request)
	{
		$this->validate($request, [
			'first_name' => 'required|max:255',
			'last_name'  => 'required|max:255',
			'email'      => 'required|email|max:255|unique:users',
			'account' => 'required',
		]);

		$user = new User;
		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->email = $request->email;
		$user->account_id = $request->account;
		$user->timezone = 'Australia/Brisbane';

		$user->save();

		$this->addTimeline([
			'created_by' => Auth::user()->id,
			'user_id'    => $user->id,
			'account_id'    => $user->account_id,
			'title'		 => 'Invited User',
			'message'    => Auth::user()->email . ' invited user '.ucwords($request->first_name.' '.$request->last_name).' ('.$request->email.')',
			'type'       => 'info'
		]);

		$invitation = new Invitation;
		$invitation->token = substr(md5(microtime()), 0, 10);
		$invitation->user_id = $user->id;
		$invitation->save();

		$data = [
			'user'       => $user,
			'invitation' => $invitation,
		];

		$this->sendInviteEmail($user,$data);

		return $user;
	}

	/**
	 * Send invitation email to user
	 * @param  string $email
	 * @param  array $data
	 * @return null
	 */
	public function sendInviteEmail($user,$data)
	{
		try {
			Mail::send('emails.invitations-email', $data, function ($mail) use ($user) {
				$mail->from('admin@buckhamduffy.com');
				$mail->to($user->email);
				$mail->subject('Invitation to join Superglue');
				die(var_dump($mail));
			});
		} catch (Exception $e) {
			Log::info('Sending invite email error : '.$e->getMessage());
		}
	}

	/**
	 * Resend invitation email to user
	 * @param  Request $request
	 * @return null
	 */
	public function resendInvite(Request $request)
	{
		$user = User::findOrFail($request->user_id);
		$invitation = $user->invitations->first();

		if ($invitation) {

			$data = [
				'user'       => $user,
				'invitation' => $invitation,
			];

			$this->sendInviteEmail($user,$data);
			$invitation->touch();
		}

		$this->addTimeline([
			'created_by' => Auth::user()->id,
			'user_id'    => $request->user_id,
			'title'      => 'Re-sent invitation email',
			'message'    => 'Re-sent invitation email to: '.$user->email,
			'type'       => 'info'
		]);
	}

	/**
	 * Create created user details from user create page
	 * @param  Request $request
	 * @return null
	 */
	public function create(Request $request)
	{
		$validation_array = [
			'first_name' => 'required',
			'last_name' => 'required',
			'phone' => '',
			'company_name' => '',
			'job_title' => '',
			'dob'   =>  'date_format:Y-m-d',
			'like_tour' => 'boolean',
			'has_visited' => 'boolean',
			'accepts_terms' => 'boolean',
			'type' => 'required',
		];
		if (!isset($request->linkedin_token)) {
			$validation_array['password'] = 'required|confirmed';
			$validation_array['password_confirmation'] = 'required';
		}

		$validation_array['email'] = $request->invitation_token ? 'required|email' : 'required|email|unique:users';

		$this->validate($request,$validation_array);

		if($request->invitation_token) {
			$invitation = Invitation::where('token', $request->invitation_token)->orderBy('created_at', 'desc')->first();
			$user = $invitation->user;
		} else {
			$user = new User;
		}

		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->email = $request->email;
		$user->dob = $request->dob;
		$user->linkedin_token = $request->get('linkedin_token', '');
		$user->company_name = $request->get('company_name', '');
		$user->job_title = $request->get('job_title', '');
		$user->industry = $request->get('industry', '');
		$user->phone = $request->get('phone', '');
		$user->address = $request->get('address', '');
		$user->twitter_handle = $request->get('twitter_handle', '');
		$user->instagram_handle = $request->get('instagram_handle', '');
		$user->bio = $request->get('bio', '');
		$user->type = $request->type;
		$user->accepts_terms = $request->get('accepts_terms', '0');
		$user->like_tour = $request->get('like_tour', '0');
		$user->has_visited = $request->get('has_visited', '0');
		$user->password = bcrypt($request->password);
		//temporarily set all timezones into Australia/Brisbane
		$user->timezone = $request->get('timezone','Australia/Brisbane');

		$account = new Account;
		$account->name = $request->first_name . ' ' . $request->last_name;
		$account->email = $request->email;
		$account->space_id = $request->get('space_id', Space::first()->id);
		$account->save();

		$user->account_id = $account->id;

		$user->save();

		if($request->invitation_token) {
			$invitation->delete();
		}

		$info = [
			'created_by' => Auth::user() ? Auth::user()->id : null,
			'user_id' => $user->id,
			'account_id' => $account->id,
			'title'		=> 'User Signup',
			'message' => ucwords($request->first_name.' '.$request->last_name).' ('.$request->email.')' . ' signed up.',
			'type' => 'info'
		];
		$this->addTimeline($info);

		return $user;
	}

	public function createFromInvite($request,$user_id)
	{
		$validation_array = [
			'first_name' => 'required',
			'last_name' => 'required',
			'phone' => 'required',
			'company_name' => 'required',
			'job_title' => 'required',
			'dob'   =>  'required|date_format:Y-m-d',
			'password' => 'required|confirmed',
			'password_confirmation' => 'required',
			'postcode' => 'required|numeric',
			'industry' => 'required',
		];
		$this->validate($request,$validation_array);

		$user = User::find($user_id);

		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->password = bcrypt($request->password);
		$user->dob = $request->dob;
		$user->postcode = $request->get('postcode', '');
		$user->job_title = $request->get('job_title', '');
		$user->company_name = $request->get('company_name', '');
		$user->industry = $request->get('industry', '');
		$user->phone = $request->get('phone', '');
		$user->twitter_handle = $request->get('twitter_handle', '');
		$user->instagram_handle = $request->get('instagram_handle', '');
		$user->bio = $request->get('bio', '');
		//temporarily set all timezones into Australia/Brisbane
		$user->timezone = $request->get('timezone','Australia/Brisbane');

		$user->save();

		$info = [
			'created_by' => Auth::user() ? Auth::user()->id : null,
			'user_id' => $user->id,
			'account_id' => $user->account,
			'message' => 'User completed invite: '.ucwords($request->first_name.' '.$request->last_name).' ('.$user->email.')',
			'type' => 'info'
		];
		$this->addTimeline($info);

		if($request->hasfile('avatar')) {
			$avatarService = new AvatarService;
			$user->avatar = $avatarService->postAvatar($request,$user);
		}

		return $user;
	}

	/**
	 * Update user from user edit page
	 * @param  Request $request
	 * @param  int  $user_id
	 * @return null
	 */
	public function edit(Request $request,$user_id)
	{
		$validation_array = [
			'first_name'=> 'required',
			'last_name' => 'required',
			'email' 	=> 'required|email|unique:users,email,'.$user_id,
			'dob'   	=> 'date_format:Y-m-d',
		];

		$this->validate($request,$validation_array);

		$user = User::findOrFail($user_id);

		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->email = $request->email;

		if (Auth::user() && Auth::user()->isAdmin()) {
			$user->type = $request->type;
		}

		$user->dob = $request->dob;
		$user->company_name = $request->get('company_name', '');
		$user->job_title = $request->get('job_title', '');
		$user->industry = $request->get('industry', '');
		$user->phone = $request->get('phone', '');
		$user->address = $request->get('address', '');
		$user->twitter_handle = $request->get('twitter_handle', '');
		$user->instagram_handle = $request->get('instagram_handle', '');
		$user->bio = $request->get('bio', '');
		if ($request->password) {
			$user->password = bcrypt($request->password);
		}
		//temporarily set all timezones into Australia/Brisbane
		$user->timezone = 'Australia/Brisbane';

		$user->save();

		return $user;
	}

	public function addHighFive($request, $user_id)
	{
		$user = User::findOrFail($user_id);
		$sender = isset($request->api_token)? Auth::guard('api')->user() : Auth::user();
		$high_five = HighFive::where('user_id', $user->id)
						->where('created_by', $sender->id)
						->where('created_at', '>=', (new Carbon('today', $user->timezone))->setTimezone('UTC'))
						->first();
		if ($high_five) {
			throw new	ServiceValidationException('High Five already sent for today.');
		}
		$high_five = new HighFive;
		$high_five->user_id = $user->id;
		$high_five->created_by = $sender->id;
		$high_five->created_at = Carbon::now();
		$high_five->save();

		$user->num_high_fives = $user->num_high_fives + 1;
		$user->save();
		return true;
	}
}
