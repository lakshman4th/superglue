<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use Auth;

class PostService extends Service
{
	/**
	 * create post
	 * @param  Request $request 
	 * @return object           
	 */
	public function create(Request $request,$feed_id)
	{
		$request->merge(['feed_id' => $feed_id]);

		$this->validate($request,[
				'feed_id' => 'required|exists:feeds,id',
				'content' => 'required'
			],[
				'feed_id.exists' => 'The feed id does not exist in the database.',
			]);

		$user = $request->api_token ? Auth::guard('api')->user() : Auth::user();

		$post = new Post;
		$post->created_by = $user->id;
		$post->feed_id = $feed_id;
		$post->unique_id = md5(microtime());
		$post->content = $request->content;
		$post->save();

		return $post;
	}
}
