<?php

namespace App\Services;

use Illuminate\Http\Request;

use App\HolidayPeriod;

class HolidayService extends Service
{
	/**
	 * Create holiday period for account
	 * @param  Request $request    
	 * @param  int  $account_id 
	 * @return null              
	 */
	public function add(Request $request,$account_id)
	{
		$this->validate($request,[
				'start_date' => 'required|date_format:Y-m-d',
				'end_date' => 'required|date_format:Y-m-d',
			]);

		$holiday = new HolidayPeriod;
		$holiday->account_id = $account_id;
		$holiday->start_date = $request->start_date;
		$holiday->end_date = $request->end_date;
		$holiday->save();

	}

	/**
	 * Edit holiday period for account
	 * @param  Request $request    
	 * @param  int  $holiday_id 
	 * @return null              
	 */
	public function edit(Request $request,$holiday_id)
	{
		$this->validate($request,[
				'start_date' => 'required|date_format:Y-m-d',
				'end_date' => 'required|date_format:Y-m-d',
			]);

		$holiday = HolidayPeriod::find($holiday_id);
		$holiday->start_date = $request->start_date;
		$holiday->end_date = $request->end_date;
		$holiday->save();
	}
}
