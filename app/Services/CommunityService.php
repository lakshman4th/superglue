<?php

namespace App\Services;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Community;

class CommunityService
{
	/**
	 * Create community
	 * @param  Request $request 
	 * @return null           
	 */
    public function createCommunity(Request $request)
    {
    	$community = new Community;
    	$community->name = $request->community_name;
    	$community->save();
    }

    /**
     * Eedit community
     * @param  Request $request      
     * @param  int  $community_id 
     * @return null                
     */
    public function editCommunity(Request $request,$community_id)
    {
    	$community = Community::find($community_id);
    	$community->name = $request->community_name;
    	$community->save();
    }

    /**
     * Delete community
     * @param  int $community_id 
     * @return null               
     */
    public function deleteCommunity($community_id)
    {
    	Community::where('id',$community_id)->delete();
    }
}
