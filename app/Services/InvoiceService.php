<?php

namespace App\Services;

use Illuminate\Http\Request;

use App\Invoice;
use App\Payment;

class InvoiceService extends Service
{
	/**
	 * Updates status of invoice. If total payments is equal or greater than the invoice amount, then set status to 'paid'. 'pending' otherwise.
	 * @param  int $invoice_id 
	 * @return null             
	 */
	public function updateStatus($invoice_id)
	{
		$invoice = Invoice::findOrFail($invoice_id);
		$total_payments = Payment::selectRaw('sum(amount) as total')->where('invoice_id',$invoice_id)->first()->total;

		$invoice->status = $invoice->total <= $total_payments ? 'paid' : 'pending';

		$invoice->save();
	}
}
