<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\BillingItem;
use Auth;
use Carbon\Carbon;
use App\Account;
use App\Plan;

class AccountService extends Service
{
	public function delete(Request $request,$account_id)
	{
		$this->validate($request,[
				'termination_date' => 'required|date_format:Y-m-d'
			]);

		$query = BillingItem::query();
		$query->where('account_id',$account_id)
				->where(function($query) use ($request){
					$query->orWhere('end_date',null);
					$query->orWhere('end_date','>',$request->termination_date);
				})->update([
					'end_date' => $request->termination_date
				]);
	}

	public function updateCreditCard(Request $request)
	{
		$this->validate($request,[
				'stripe_customer_token' => 'required',
				'card_brand' 			=> 'required',
				'card_last_four' 		=> 'required',
			]);

		$account_id = Auth::guard('api')->user()->account->id;
		$account = Account::findOrNew($account_id);
		$account->stripe_id = $request->stripe_customer_token;
		$account->card_brand = $request->card_brand;
		$account->card_last_four = $request->card_last_four;
		$account->save();

		return $account;
	}

	public function addMembership(Request $request)
	{
		$this->validate($request,[
				'plan_id' => 'required',
			]);

		$account_id = Auth::guard('api')->user()->account->id;

		$plan = Plan::find($request->plan_id);

		$item = new BillingItem;
		$item->account_id = $account_id;
		$item->plan_id = $request->plan_id;
		$item->name = 'Plan: '.$plan->name;
		$item->cost = $plan->cost;
		$item->num_credits = $plan->credit_per_renewal;
		$item->start_date = Carbon::now();
		$item->save();

		return $item;
	}	

	public function purchaseCredit(Request $request,$account_id)
	{

		$this->validate($request,[
				'cost' => 'required|numeric',
			]);

		$account = Account::findOrFail($account_id);

		$item = new BillingItem;
		$item->account_id = $account_id;
		$item->name = 'Credit Top-up';
		$item->cost = $request->cost;
		$item->start_date = Carbon::now();
		$item->end_date = Carbon::now();
		$item->save();

		$debit_cost = $request->cost * -1;
		$account->debit($debit_cost,'Credit Top-up');


		return $item;
	}
}
