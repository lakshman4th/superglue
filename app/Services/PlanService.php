<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Plan;

class PlanService extends Service
{
	/**
	 * @param  Request $request
	 * @param  int  $plan_id
	 * @return null
	 */
	public function save(Request $request, $plan_id = null)
	{
		$this->validate($request,[
			'name'               => 'required',
			'num_seats'          => 'required|numeric',
			'credit_per_renewal' => 'required|numeric',
			'cost'               => 'required|numeric',
			'setup_cost'         => 'required|numeric',
		]);

		$plan = Plan::findOrNew($plan_id);

		$plan->name = $request->name;
		$plan->num_seats = $request->num_seats;
		$plan->credit_per_renewal = $request->credit_per_renewal;
		$plan->cost = $request->cost;
		$plan->setup_cost = $request->setup_cost;

		$plan->save();
	}

	/**
	 * @param  Request $request
	 * @return null
	 */
	public function delete(Request $request)
	{
		Plan::where('id', $request->plan_id)->delete();
	}
}
