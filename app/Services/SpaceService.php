<?php

namespace App\Services;

use Illuminate\Http\Request;

use App\Space;

class SpaceService
{
    /**
     * Save space details
     * @param  Request $request 
     * @return null           
     */
    public function createSpace(Request $request)
    {
        $space = new Space;
        $space->name = $request->get('space_name','');
        $space->address = $request->get('address','');
        $space->suburb = $request->get('suburb','');
        $space->postcode = $request->get('postcode','');
        $space->state = $request->get('state','');
        $space->country = $request->get('country','');
        $space->timezone = $request->get('timezone','');
        $space->save();
    }

    /**
     * Edit space details
     * @param  Request $request  
     * @param  int  $space_id 
     * @return null            
     */
    public function editSpace(Request $request,$space_id)
    {
        $space = Space::find($space_id);
        $space->name = $request->get('space_name','');
        $space->address = $request->get('address','');
        $space->suburb = $request->get('suburb','');
        $space->postcode = $request->get('postcode','');
        $space->state = $request->get('state','');
        $space->country = $request->get('country','');
        $space->timezone = $request->get('timezone','');
        $space->save();
    }

    /**
     * Delete Space
     * @param  int $space_id 
     * @return null           
     */
    public function deleteSpace($space_id)
    {
        Space::where('id',$space_id)->delete();
    }
}
