<?php

namespace App\Services;

use Illuminate\Http\Request;
use Storage;


class AvatarService extends Service
{
	/**
	 * Store avatar file 
	 * @param  Request $request
	 * @param  $user
	 * @return avatar image
	 */
	public function postAvatar(Request $request,$user)
	{
		$extension = $request->file('avatar')->getClientOriginalExtension();
		$unique_id = $user->id . '-' . substr(md5(microtime()), 0, 10);
		$filename = $unique_id . '.' . $extension;

		Storage::disk('public')->put('avatars/' . $filename, file_get_contents($request->file('avatar')));
		

		if($user->avatar != '') {
			Storage::disk('public')->delete('avatars/'.$user->avatar);
		}

		return $filename;
	}
}
