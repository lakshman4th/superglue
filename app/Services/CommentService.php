<?php

namespace App\Services;

use Illuminate\Http\Request;
use Auth;
use App\Comment;
use App\User;

class CommentService extends Service
{
	/**
	 * Create comment entry for post
	 * @param  Request $request 
	 * @param  int  $post_id 
	 * @return object           
	 */
	public function create(Request $request,$post_id)
	{
		$request->merge(['post_id' => $post_id]);

		$this->validate($request,[
				'post_id' => 'required|exists:posts,id',
				'content' => 'required',
			],[
				'post_id.exists' => 'The post id does not exist in the database.',
			]);

		$user = $request->api_token ? Auth::guard('api')->user() : Auth::user();

		$comment = new Comment;
		$comment->user_id = $user->id;
		$comment->post_id = $post_id;
		$comment->content = $request->content;
		$comment->save();

		return $comment;
	}

	/**
	 * Update comment entry for post
	 * @param  Request $request    
	 * @param  int  $comment_id 
	 * @return object              
	 */
	public function update(Request $request,$comment_id)
	{
		$this->validate($request,[
				'content' => 'required',
			]);

		$comment = Comment::findOrFail($comment_id);
		$comment->content = $request->content;
		$comment->save();

		return $comment;
	}
}
