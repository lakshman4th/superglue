<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Like;
use App\User;
use App\Post;
use Auth;

class LikeService extends Service
{
	/**
	 * Create like entry for post. Also increments value on num_likes for posts table
	 * @param  Request $request 
	 * @param  int  $post_id 
	 * @return object           
	 */
	public function create(Request $request,$post_id)
	{
		$user = $request->api_token ? Auth::guard('api')->user() : Auth::user();

		$request->merge(['post_id' => $post_id]);
		$request->merge(['user_id' => $user->id]);

		$this->validate($request,[
				'post_id' => 'required|exists:posts,id',
				'user_id' => 'required|unique:likes,user_id,NULL,id,post_id,'.$post_id,
			],[
				'post_id.exists' => 'The post id does not exist in the database.',
				'user_id.unique' => 'User has already liked the post.',
			]);

		$like = new Like;
		$like->user_id = $user->id;
		$like->post_id = $post_id;
		$like->save();

		Post::increment('num_likes');

		return $like;
	}

	/**
	 * Remove like from post. Also decrements value on num_likes for posts table
	 * @param  Request $request 
	 * @param  int  $post_id 
	 * @return null           
	 */
	public function delete(Request $request,$post_id)
	{
		$user = $request->api_token ? User::where('api_token',$request->api_token)->first() : Auth::user();

		$request->merge(['post_id' => $post_id]);
		$request->merge(['user_id' => $user->id]);

		$this->validate($request,[
				'post_id' => 'required|exists:posts,id|exists:likes,post_id,user_id,'.$user->id,
				'user_id' => 'required|exists:likes,user_id,post_id,'.$post_id,
			],[
				'exists' => 'The :attribute does not exist in the database.',
			]);

		$like = Like::where('user_id',$user->id)->where('post_id',$post_id)->first();
		if (!$like) {
			return;
		}
		$like->delete();
		Post::decrement('num_likes');
	}
}
