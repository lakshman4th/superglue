<?php
namespace App\Services;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use App\Timeline;

class Service extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * Create timeline entry
     * @param array $info array for details
     * Possible values:
     * int $created_by
     * int $user_id
     * int $account_id
     * string $title
     * string $message
     * string $type ['info','system','alert']
     */
    public function addTimeline($info)
    {
    	$timeline = new Timeline;

		$timeline->created_by = isset($info['created_by']) ? $info['created_by'] : null;
		$timeline->user_id = isset($info['user_id']) ? $info['user_id'] : null;
		$timeline->account_id = isset($info['account_id']) ? $info['account_id'] : null;
        $timeline->title = isset($info['title']) ? $info['title'] : '';
		$timeline->message = isset($info['message']) ? $info['message'] : '';
		$timeline->type = isset($info['type']) ? $info['type'] : 'info';

		$timeline->save();
    }
}
