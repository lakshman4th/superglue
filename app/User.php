<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use PushNotificationQueue;
use Carbon\Carbon;

class User extends Authenticatable
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name', 'last_name', 'email', 'password', 'timezone'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	protected $dates = [
		'last_login_at',
	];

	public function invitations()
	{
		return $this->hasMany('App\Invitation');
	}

	public function timeline()
	{
		return $this->hasMany('App\Timeline');
	}

	public function bookings()
	{
		return $this->hasMany('App\Booking');
	}

	/**
	 * Get devices associated with the user.
	 */
	public function devices()
	{
		return $this->hasMany('App\Device');
	}

	/**
	 * Get work history items associated with the user.
	 */
	public function WorkHistoryItems()
	{
		return $this->hasMany('App\WorkHistoryItem');
	}

	/**
	 * Get the company record associated with the user.
	 */
	public function company()
	{
		return $this->belongsTo('App\Company');
	}

	/**
	 * Get the account record associated with the user.
	 */
	public function account()
	{
		return $this->belongsTo('App\Account');
	}

	/**
	 * Get list of users with filters
	 * @param  query $query
	 * @param  array $filter
	 * @return array
	 */
	public function scopeFilters($query,$filter = null)
	{
		$query->select('users.id','users.first_name','users.last_name','users.email','users.type',
			'users.created_at','users.last_login_at');
		$query->where(function($query) use ($filter)
		{
			$query->orWhere('first_name','like','%'.$filter['global_search'].'%');
			$query->orWhere('last_name','like','%'.$filter['global_search'].'%');
			$query->orWhere('email','like','%'.$filter['global_search'].'%');
		});
		if(isset($filter['user_type']) && $filter['user_type'] !='All'){
			
			 $query->Where('users.type',$filter['user_type']);

		}

		return $query->orderBy('id','desc')->paginate(25);
	}

	public function isAdmin()
	{
		return $this->type == 'Admin';
	}

	public function isMentor()
	{
		return $this->type == 'Mentor';
	}

	/**
	 * will determine if the account the user belongs to has an active membership in the billing items.
	 *
	 * @return bool
	 */
	public function isMember()
	{
		// check if any billing items is associated with account.
		return BillingItem::where('account_id',$this->account->id)
							->where(function($query)
							{
								$query->where('end_date','>=',Carbon::now())
									  ->orWhereNull('end_date');
							})
							->whereNotNull('plan_id')
							->orWhereNotNull('desk_id')
							->orWhereNotNull('office_id')
							->exists();
	}


	/**
	 * Get the user's avatar as a url.
	 *
	 * @return string
	 */
	public function getAvatarUrl()
	{
		if($this->avatar != '') {
			return url('/avatars') . '/' . $this->avatar;
		}
	}


	public function highFives()
	{
		return $this->hasMany('App\HighFive');
	}

	/**
	 * Send Push Notifications
	 *
	 * @return string
	 */
	public function sendPushNotification($alert, $badge, $link_url)
	{
		$devices = Device::whereUserId($this->id)->get();

		$notifications = [];
		foreach($devices as $device) {
			$notifications[] = [
				'device_id'	=>	$device->id,
				'alert'	=>	$alert,
				'link_url'	=>	$link_url,
				'badge'	=>	$badge,
				'created_at'	=>	Carbon::now()
			];
		}

		PushNotificationQueue::create($notifications);
		return true;
	
	}
}
