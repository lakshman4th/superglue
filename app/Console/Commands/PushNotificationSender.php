<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PushNotificationQueue;

class PushNotificationSender extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'notification:send';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sends Push Notifications.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		while ($item = PushNotificationQueue::first()) {
			$this->sendNotification($item);
			$item->delete();
		}
	}

	private function sendNotification($notification)
	{
		$device = Device::findOrFail($notification->device_id);
		$message = \PushNotification::Message($notification->alert, ['alert' => $notification->alert, 'link_url' => $notification->link_url, 'badge' => $notification->badge]);

		$collection = \PushNotification::app('appNameAndroid')
						->to($device->token)
						->send($message);
	}
}