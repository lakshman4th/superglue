<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account;
use App\Invoice;
use App\InvoiceItem;
use Carbon\Carbon;
use DB;

class Billing extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'billing';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Process billing for all accounts.';

	/**
	 * Execute the console command.
	 *
	 * This command can be run on a billing day, as well as on every day in
	 * between. On a billing day it creates invoices and attempts to pay them.
	 * On non-billing days it just tries to process unpaid invoices.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		if ($this->isBillingDay()) {
			$this->createInvoices();
		}

		$this->payInvoices();
		$this->expireAccounts();
	}

	/**
	 * Determines if today is a billing day.
	 */
	private function isBillingDay()
	{
		return true;
		$today = Carbon::today();
		$billing_date = new Carbon(env('BILLING_DATE_REFERENCE'));

		while ($billing_date < $today) {
			$billing_date->addWeeks(2);
		}

		return $today->eq($billing_date);
	}

	/**
	 * Creates invoices based on each account's billing items.
	 */
	private function createInvoices()
	{
		$today = Carbon::today();
		$next_billing_date = new Carbon(env('BILLING_DATE_REFERENCE'));

		while ($next_billing_date <= $today) {
			$next_billing_date->addWeeks(2);
		}

		foreach (Account::all() as $account) {
			$items = [];
			$total = 0;

			foreach ($account->billingItems as $item) {
				if ($item->start_date > $today) {
					continue;
				}

				$items[] = $item;
				$total += $item->cost;
			}

			if (!$items) {
				continue;
			}

			$invoice = Invoice::create([
				'account_id' => $account->id,
				'total'      => $total,
				'status'     => 'pending',
				'due_date'   => $today,
			]);

			foreach ($items as $item) {
				InvoiceItem::create([
					'invoice_id'  => $invoice->id,
					'description' => $item->name,
					'cost'        => $item->cost,
					'num_credits' => $item->num_credits,
				]);

				// Update the billing item's next billing date, or delete it
				if (!$item->end_date || $item->end_date >= $next_billing_date) {
					$item->start_date = $next_billing_date;
					$item->save();
				} else {
					$item->delete();
				}
			}
		}
	}

	/**
	 * Loops through all unpaid invoices and attempts to pay them.
	 */
	private function payInvoices()
	{
		$invoices = Invoice::whereRaw('due_date <= CURDATE()')
		                   ->whereStatus('pending')
		                   ->get();

		foreach ($invoices as $invoice) {
			$invoice->pay();
		}
	}

	/**
	 * Sets account statuses to expired if they have an unpaid invoice over 14
	 * days old.
	 */
	private function expireAccounts()
	{
		DB::table('invoices AS i')
		  ->join('accounts AS a', 'i.account_id', '=', 'a.id')
		  ->whereRaw('i.due_date <= DATE_SUB(CURDATE(), INTERVAL 14 DAY)')
		  ->where('i.status', '=', 'pending')
		  ->update(['a.status' => 'expired']);
	}

}
