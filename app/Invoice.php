<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
use Stripe;

class Invoice extends Model
{

	protected $dates = ['due_date'];

	protected $guarded = [];

	public function pay()
	{
		$account = $this->account;

		$charge = Stripe::charges()->create([
			'customer'             => $account->stripe_id,
			'currency'             => 'AUD',
			'amount'               => $this->total,
			'description'          => 'Invoice #' . $this->id,
			'statement_descriptor' => 'Invoice #' . $this->id,
			'receipt_email'        => $account->email,
		]);

		if (!$charge['paid']) {
			Log::warning('Payment for invoice #' . $this->id . ' failed: ' . $charge['failure_message']);
			return false;
		}

		$this->status = 'paid';
		$this->save();

		Log::info('Payment for invoice #' . $this->id . ' succeeded');

		// Reset/apply credit
		$account->credit_balance = 0;

		foreach ($this->items as $item) {
			$account->credit_balance += $item->num_credits;
		}

		$account->save();

		return true;
	}

	public function items()
	{
		return $this->hasMany('App\InvoiceItem');
	}

	public function account()
	{
		return $this->belongsTo('App\Account');
	}

	public function payments()
	{
		return $this->hasMany('App\Payment');
	}

}
