<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    public function scopeIndustries()
    {
    	return ['IT', 'Creative', 'Manufacturing', 'Professional Services'];
    }
}
