<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Public Routes
Route::auth();
Route::controller('invitations', 'InvitationsController');

Route::get('/access-denied', function() {
	return "Access Denied";
});

Route::group(['middleware' => ['auth']], function () {

	Route::get('/', 'AnnouncementsController@getIndex');

	Route::controller('bookings', 'BookingsController');
	Route::controller('events', 'EventsController');
	Route::controller('members', 'MembersController');
	Route::controller('account', 'AccountController');
	Route::controller('billing-items', 'BillingItemsController');
	Route::controller('profile', 'ProfileController');
	Route::controller('invoices', 'InvoicesController');
	Route::controller('work-history', 'WorkHistoryController');

});

// Admin routes
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth','admin']], function() {
	Route::get('/', function()
	{
	    return View::make('pages.home');
	});
	Route::controller('users', 'UsersController');
	Route::controller('spaces', 'SpacesController');
	Route::controller('offices', 'OfficesController');
	Route::controller('desks', 'DesksController');
	Route::controller('rooms', 'RoomsController');
	Route::controller('companies', 'CompaniesController');
	Route::controller('communities', 'CommunitiesController');
	Route::controller('events', 'EventsController');
	Route::controller('event-requests', 'EventRequestsController');
	Route::controller('mentor-requests', 'MentorRequestsController');
	Route::controller('announcements', 'AnnouncementsController');
	Route::controller('accounts', 'AccountsController');
	Route::controller('plans', 'PlansController');
	Route::controller('holidays', 'HolidaysController');
	Route::controller('payments', 'PaymentsController');
	Route::controller('billing-items', 'BillingItemsController');
	Route::controller('refer-friends', 'ReferFriendsController');
	Route::controller('powerups', 'PowerUpsController');

});

// API routes
Route::group(['prefix' => 'api', 'namespace' => 'Api'], function() {
	// Token not required
	Route::controller('auth', 'AuthController');
	Route::controller('resources', 'ResourcesController');

	//Some functions need the token
	Route::controller('users', 'UsersController');

	// Token required
	Route::group(['middleware' => 'auth:api'], function() {
		Route::controller('account', 'AccountController');
		Route::controller('mentors', 'MentorsController');
		Route::controller('members', 'MembersController');
		Route::controller('spaces', 'SpacesController');
		Route::controller('posts', 'PostsController');
		Route::controller('comments', 'CommentsController');
		Route::controller('likes', 'LikesController');

		Route::controller('announcements', 'AnnouncementsController');
		Route::controller('powerups', 'PowerUpsController');

		Route::get('events/', 'EventsController@getList');
		Route::get('events/request-event', 'EventsController@getRequestEvent');
		Route::get('events/{event_id}', 'EventsController@getDetail');
		Route::post('events/{event_id}/update-attendance', 'EventsController@postUpdateAttendance');

		Route::get('bookings/init', 'BookingsController@getInit');
		Route::get('bookings/calendar', 'BookingsController@getCalendar');
		Route::get('bookings/my', 'BookingsController@getMy');
		Route::post('bookings/create', 'BookingsController@postCreate');
		Route::post('bookings/{booking_id}/edit', 'BookingsController@postEdit');
		Route::post('bookings/{booking_id}/delete', 'BookingsController@postDelete');
	});
});
