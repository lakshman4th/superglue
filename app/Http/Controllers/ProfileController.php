<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Industry;
use App\Services\UserService;

class ProfileController extends Controller
{
	/**
	 * Show user profile page
	 * @param  int $user_id
	 * @return view
	 */
	public function getIndex()
	{
		$user = Auth::user();

		$title = $user->name . ' Profile';

		return view('pages.public.profile-view')
				->with('user', $user)
				->with('title', $title);
	}

	/**
	 * Show edit user page
	 * @param  int $user_id
	 * @return view
	 */
	public function getEdit()
	{
		$user = Auth::user();
		$industries = Industry::industries();

		return view('pages.users-edit')
				->with('user', $user)
				->with('industries', $industries)
				->with('title', 'Edit User');
	}

	/**
	 * Handle edit user data
	 * @param  Request $request
	 * @param  int  $user_id
	 * @return redirect
	 */
	public function postEdit(Request $request)
	{
		$this->validate($request,[
				'dob' => 'date_format:d/m/Y'
			]);
		$new_dob = $request->dob ? Carbon::createFromFormat('d/m/Y', $request->dob)->format('Y-m-d') : null;
		$request->merge(['dob' => $new_dob]);

		$userService = new UserService;
		$userService->edit($request,Auth::user()->id);

		return redirect('/profile');
	}
}
