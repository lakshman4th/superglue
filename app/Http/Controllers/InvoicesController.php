<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Invoice;
use Auth;

class InvoicesController extends Controller
{
	public function getView($id)
	{
		$invoice = Invoice::whereAccountId(Auth::user()->account->id)->find($id);

		if(!$invoice){
			return redirect('/access-denied');
		}

		$pdf = \PDF::loadView('invoices.invoice', ['invoice' => $invoice]);
		return $pdf->stream('invoice.pdf');
		return view('invoices.invoice')
				->with('invoice', $invoice);
				
	}
}
