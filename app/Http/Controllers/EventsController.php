<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class EventsController extends Controller
{
    public function getIndex()
    {

    	$events = [];
    	return view('pages.public.events-list')
    			->with('events', $events)
    			->with('title', "Events List");
    }
}
