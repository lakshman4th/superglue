<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Services\UserService;
use Carbon\Carbon;
use DB;
use App\User;
use App\Payment;
use App\Plan;
use App\BillingItem;
use App\CreditTransaction;

class AccountController extends Controller
{

	public function getIndex()
	{
		$user = User::findOrFail(Auth::user()->id);

		$invoices = DB::table('invoices AS i')
						->join('payments AS p', 'p.invoice_id', '=', 'i.id')
						->groupBy('i.id')
						->where('i.account_id',$user->account_id)
						->selectRaw("i.*, SUM(p.amount) AS amount_paid")
						->orderBy('i.id','desc')
						->get();

		$payments = Payment::where('account_id',$user->account_id)
						->orderBy('id','desc')
						->get();

		$billing_items = BillingItem::where('account_id',$user->account_id)
						->orderBy('id','desc')
						->get();

		$credit_transactions = CreditTransaction::where('account_id',$user->account_id)
						->orderBy('id','desc')
						->limit(10)
						->get();

		$plans = Plan::orderBy('name')->get();

		return view('pages.public.account-profile')
				->with('invoices',$invoices)
				->with('payments',$payments)
				->with('billing_items',$billing_items)
				->with('credit_transactions',$credit_transactions)
				->with('plans',$plans)
				->with('user', $user)
				->with('title', 'Account');
	}

	public function postCard(Request $request, $account_id)
	{
		$this->validate($request, [
			'card_token' => 'required',
		]);

		$account = Account::findOrFail($account_id);
		$account->setCard($request->card_token);
	}


	public function getEdit()
	{
		$user = Auth::user();
		return view('pages.public.account-edit')
				->with('user', $user)
				->with('title', 'Edit Profile');
	}

	/**
	 * Handle edit user data
	 * @param  Request $request
	 * @return redirect
	 */
	public function postEdit(Request $request)
	{
		$this->validate($request,[
				'dob' => 'date_format:d/m/Y'
			]);

		$new_dob = $request->dob ? Carbon::createFromFormat('d/m/Y', $request->dob)->format('Y-m-d') : null;
		$request->merge(['dob' => $new_dob]);

		$userService = new UserService;
		$userService->edit($request,Auth::user()->id);
		return redirect('/account');
	}

	public function getMembership()
	{
		$membership = Auth::user()->membership;

		if(!$membership) {
			return view('pages.public.account-membership-upsell');
		}

		return view('pages.public.account-membership')
				->with('membership', $membership);
	}


}
