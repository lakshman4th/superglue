<?php

namespace App\Http\Controllers;

use App\Invitation;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Hash;
use Auth;
use App\User;
use App\Industry;
use App\Services\UserService;
use Carbon\Carbon;

class InvitationsController extends Controller
{
	/**
	 * Display Dashboard
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getAccept($token)
	{
		$invitation = Invitation::whereToken($token)->first();

		if (!$invitation) {
			abort(404);
		}

		$user = User::find($invitation->user_id);

		if (!$user) {
			abort(404);
		}

		$industries = Industry::industries();

		return view('auth.invitations_accept')
		     ->with('invitation', $invitation)
		     ->with('industries', $industries)
		     ->with('user', $user);
	}

	public function postAccept(Request $request, $token)
	{
		$invitation = Invitation::whereToken($token)->first();

		if (!$invitation) {
			abort(404);
		}

		$this->validate($request,[
				'dob' => 'date_format:d/m/Y'
			]);

		$new_dob = $request->dob ? Carbon::createFromFormat('d/m/Y', $request->dob)->format('Y-m-d') : null;
		$request->merge(['dob' => $new_dob]);

		$userService = new UserService;
		$user = $userService->createFromInvite($request,$invitation->user_id);



		$invitation->delete();

		Auth::login($user);

		return redirect('/');
	}

}
