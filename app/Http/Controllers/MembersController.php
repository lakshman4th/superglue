<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class MembersController extends Controller
{
	public function getIndex()
	{
		$members = User::where('type', 'Member')->paginate(12);

		return view('pages.public.members-list')
				->with('members', $members)
				->with('title', "Members List");
	}
}
