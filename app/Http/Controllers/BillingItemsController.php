<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Services\BillingItemService;
use Carbon\Carbon;
use Auth;

class BillingItemsController extends Controller
{
    public function postAdd(Request $request)
    {
		$this->validate($request,[
				'start_date' => 'date_format:d/m/Y',
			]);
		
		$start_date = $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : null;
		$request->merge(['start_date' => $start_date]);

    	$service = new BillingItemService;
    	$service->add($request,Auth::user()->account_id);
    }
}
