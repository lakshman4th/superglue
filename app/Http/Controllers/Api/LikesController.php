<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Like;
use App\Services\LikeService;

class LikesController extends Controller
{
	/**
	 * Get likes for post
	 * @param  int $post_id 
	 * @return json          
	 */
	public function getList($post_id)
	{
		$likes = Like::where('post_id',$post_id)->get();
		$result = [];
		foreach ($likes as $like) {
			$result[] = [
				'user_id' => $like->user_id,
				'user_name' => ucwords($like->user->first_name.' '.$like->user->last_name),
			];
		}

		return response()->json($result);
	}

	/**
	 * Add like to post. Also increments value on num_likes for posts table
	 * @param  Request $request 
	 * @param  int  $post_id 
	 * @return json           
	 */
	public function postCreate(Request $request,$post_id)
	{
		$likeService = new LikeService;
		$like = $likeService->create($request,$post_id);

		return response()->json($like);
	}

	/**
	 * Remove like from post. Also decrements value on num_likes for posts table
	 * @param  Request $request 
	 * @param  int  $post_id 
	 * @return json           
	 */
	public function postDelete(Request $request,$post_id)
	{
		$likeService = new LikeService;
		$likeService->delete($request,$post_id);

		return response()->json(['message' => 'Delete successful.']);
	}
}
