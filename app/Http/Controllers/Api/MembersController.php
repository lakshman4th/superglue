<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\Services\UserService;


class MembersController extends Controller
{

	/**
	 * Get list of members
	 * @return json 
	 */
	public function getIndex()
	{
		$users = User::where('type','member')->paginate(10);

		foreach($users as $user) {
			$user->avatar =  $user->getAvatarUrl();
		}

		return response()->json($users);
	}

	/**
	 * Get member profile
	 * @param  int $user_id 
	 * @return json
	 */
	public function getProfile($user_id)
	{
		$user = User::findOrFail($user_id);

		$user->avatar =  $user->getAvatarUrl();
		
		return response()->json($user);
	}

	public function postHighFive(Request $request, $user_id)
	{
		$service = new UserService;
		$result = $service->addHighFive($request, $user_id);

		return response()->json($result);
	}
}
