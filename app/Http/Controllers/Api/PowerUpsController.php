<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PowerUp;

class PowerUpsController extends Controller
{
    public function getIndex()
    {
    	$powerups = PowerUp::paginate(10);

    	return response()->json($powerups);
    }
}
