<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use \App\User;
use App\Industry;
use App\PowerUp;

class ResourcesController extends Controller
{
	public function getIndustries()
	{
		$industries = Industry::industries();

		return response()->json($industries);
	}

	public function getInvestment() {

		$investment_levels = array('<$30,000', '$30,000 - $50,000', '$50,000 - $100,000', '$100,000 - $200,000', '$200,000 - $500,000', '$500,000 - $1,000,000', '$1,000,000+');

		return response()->json($investment_levels);
	}

	public function getRevenue() {

		$revenue_levels = array('<$30,000', '$30,000 - $50,000', '$50,000 - $100,000', '$100,000 - $200,000', '$200,000 - $500,000', '$500,000 - $1,000,000', '$1,000,000+');

		return response()->json($revenue_levels);
	}

	/**
	 * Get All powerups 
	 * @return json          
	 */
    public function getPowerups(Request $request)
    {
        $powerups = PowerUp::orderBy('id','desc')->paginate(25);

        $result = [];
		foreach ($powerups as $powerup) {
			$result[] = [
				'id' => $powerup->id,
				'title' => $powerup->title,
				'description' => $powerup->description,
				'link' => $powerup->link,
				'created_at' => $powerup->created_at,
			];
		}

        return response()->json($result);
    }
}
