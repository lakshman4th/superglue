<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Storage;
use App\User;
use App\Services\AvatarService;
use App\Services\AccountService;
use App\Plan;
use App\BillingItem;
use Carbon\Carbon;

class AccountController extends Controller
{
	public function postAvatar(Request $request)
	{

		$this->validate($request, [
			'avatar' => 'required|image',
		]);

		$user = Auth::guard('api')->user();

		$avatarService = new AvatarService;
		$user->avatar = $avatarService->postAvatar($request,$user);

		$user->save();

		return response()->json('success', 200);
	}

	public function postUpdateCreditCard(Request $request)
	{
		$service = new AccountService;
		$item = $service->updateCreditCard($request);

		return response()->json($item);
	}

	public function getPlans()
	{
		$plans = Plan::orderBy('name')->get();

		return response()->json($plans);
	}

	public function postAddMembership(Request $request)
	{
		$service = new AccountService;
		$item = $service->addMembership($request);
		return response()->json($item);
	}

	public function postPurchaseCredit(Request $request,$account_id)
	{
		$service = new AccountService;
		$item = $service->purchaseCredit($request,$account_id);

		return response()->json($item);
	}

}
