<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post;
use App\Services\PostService;

class PostsController extends Controller
{
	/**
	 * Get posts for feed
	 * @param  int $feed_id 
	 * @return json          
	 */
    public function getList($feed_id)
    {
    	$posts = Post::where('feed_id',$feed_id)->paginate(10);

    	return response()->json($posts);
    }

    /**
     * Create post for feed
     * @param  Request $request 
     * @param  int  $feed_id 
     * @return object           
     */
    public function postCreate(Request $request,$feed_id)
    {
    	$postService = new PostService;
    	$post = $postService->create($request,$feed_id);

    	return $post;
    }
}
