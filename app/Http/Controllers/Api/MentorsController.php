<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\MentorRequest;
use Auth;
use Mail;


class MentorsController extends Controller
{

	/**
	 * Get list of mentors
	 * @return json 
	 */
	public function getIndex()
	{
		$users = User::where('type','mentor')->paginate(10);

		foreach($users as $user) {
			$user->avatar =  $user->getAvatarUrl();
		}

		return response()->json($users);
	}

	/**
	 * Get mentor profile
	 * @param  int $user_id 
	 * @return json          
	 */
	public function getProfile($user_id = null)
	{
		$user = User::where('type','mentor')->with('WorkHistoryItems')->findOrFail($user_id);

		$user->avatar =  $user->getAvatarUrl();

		
		return response()->json($user);
	}

	/**
	 * Book a mentor
	 * @return json          
	 */
	public function postBookMentor(Request $request)
	{ 
		$this->validate($request, [
            'mentor_id' => 'required',
            'topic' => 'required',
        ]);

        $mentor = User::where('type', 'Mentor')->findOrFail($request->mentor_id);

		$mentor_request = new MentorRequest;
		$mentor_request->member_id = Auth::guard('api')->user()->id;
		$mentor_request->mentor_id = $mentor->id;
		$mentor_request->topic = $request->topic;
		$mentor_request->save();

		$data = [
			'member'	=> Auth::guard('api')->user(),
			'topic'		=> $request->topic
		];

		$this->sendRequestEmail($mentor,$data);

		return response()->json([
				'message' => 'Requested successfully.',
			]);

	}

	public function sendRequestEmail($user,$data)
	{
		try {
			Mail::send('emails.mentor-request-email', $data, function ($mail) use ($user) {
				$mail->from('admin@littletokyotwo.com');
				$mail->to($user->email);
				$mail->subject('Request from user');
			});
		} catch (Exception $e) {
			Log::info('Sending request email error : '.$e->getMessage());
		}
	}

}
