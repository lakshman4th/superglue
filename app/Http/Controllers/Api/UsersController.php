<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use \App\User;
use \App\Device;
use \App\Invitation;
use \App\Referfriend;
use App\Services\UserService;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => [
            'postCreate',
            'getInvite',
        ]]);
    }

    public function getList()
    {
    	$users = User::all();

        foreach($users as $user) {
            $user->avatar =  $user->getAvatarUrl();
        }

		return response()->json($users);
    }

    public function getProfile($user_id = null)
    {
    	if ($user_id) {
    		$user = User::find($user_id);
    	} else {
    		$user = Auth::guard('api')->user();
    	}

    	if (!$user) {
			abort(404);
		}

        $user->avatar =  $user->getAvatarUrl();
        $user->membership_id = 1;
        $user->is_member = $user->isMember();
        $user->credit_available = $user->account->credit_balance;
        $user->valid_payment = (bool) $user->account->stripe_id;

        // Remove the account object from the results.
        unset($user->account);

        // Remove tokens
        unset($user->linkedin_token);
        unset($user->api_token);
        
		return response()->json($user);
    }

    public function getInvite($invitation_token = null)
    {
        if(!$invitation_token) {
            return response()->json(['Error' => 'No token provided']);
        }

        $invitation = Invitation::whereToken($invitation_token)->first();

        if (!$invitation) {
            return response()->json(['Error' => 'No invitation found']);
        } else {
            return response()->json($invitation->user);
        }

    }

    public function postCreate(Request $request)
    {
        $userService = new UserService;
        $user = $userService->create($request);

        return response()->json($user);

    }

    public function postEdit(Request $request, $user_id)
    {
        $userService = new UserService;
        $user = $userService->edit($request,$user_id);

        return response()->json($user);
    }


    public function getReferFriends(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'message' => 'required'
        ]);

        $refer_friend = new ReferFriend;
        $refer_friend->user_id = Auth::guard('api')->user()->id;
        $refer_friend->name = $request->get('name','');
        $refer_friend->email = $request->get('email','');
        $refer_friend->phone = $request->get('phone','');
        $refer_friend->message = $request->get('message','');
        $refer_friend->save();

        return response()->json('success');
    }

}
