<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comment;
use App\User;
use App\Services\CommentService;

class CommentsController extends Controller
{
	/**
	 * Get list for comments on post
	 * @param  int $post_id 
	 * @return json          
	 */
	public function getList($post_id)
	{
		$comments = Comment::where('post_id',$post_id)->get();
		$result = [];
		foreach ($comments as $comment) {
			$result[] = [
				'comment_id' => $comment->id,
				'user_id' => $comment->user_id,
				'user_name' => ucwords($comment->user->first_name.' '.$comment->user->last_name),
				'content' => $comment->content,
				'created_at' => $comment->created_at,
			];
		}

		return response()->json($result);
	}

	/**
	 * Create comment for post
	 * @param  Request $request 
	 * @param  int  $post_id 
	 * @return json           
	 */
	public function postCreate(Request $request,$post_id)
	{
		$commentService = new CommentService;
		$comment = $commentService->create($request,$post_id);

		return response()->json($comment);
	}

	/**
	 * Updates comment from post
	 * @param  Request $request    
	 * @param  int  $comment_id 
	 * @return json              
	 */
	public function postUpdate(Request $request,$comment_id)
	{
		$commentService = new CommentService;
		$comment = $commentService->update($request,$comment_id);

		return response()->json($comment);
	}

	/**
	 * Delete comment from post 
	 * @param  Request $request 
	 * @return json           
	 */
	public function postDelete(Request $request,$comment_id)
	{
		$user = User::where('api_token',$request->api_token)->first();
		Comment::where('id',$comment_id)->where('user_id',$user->id)->delete();

		return response()->json([
				'message' => 'Delete successful.',
			]);
	}
}
