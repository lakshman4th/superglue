<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Illuminate\Http\Request;
use App\Device;
use App\User;
use Hash;
use App\Services\AuthService;

class AuthController extends Controller
{

	public function postAuthenticate(Request $request)
	{
		$this->validate($request, [
			'email'             => 'required',
			'password'          => 'required',
		]);

		$user = User::whereEmail($request->email)->first();

		if (!$user || !Hash::check($request->password, $user->password)) {
			abort(401);
		}

		$user->api_token = md5(microtime());
		$user->save();

		return response()->json(['token' => $user->api_token]);
	}

	public function postForgotPassword(Request $request)
	{
		$authService = new AuthService;
		$user = $authService->forgotPassword($request);

		return response()->json($user);
	}

	public function postResetPassword(Request $request)
	{
		$authService = new AuthService;
		$user = $authService->resetUserPassword($request);

		return response()->json($user);
	}


 	public function postLinkedin(Request $request)
 	{
 		$this->validate($request, [
 			'access_token'             => 'required'
 		]);
 		$user = User::whereLinkedinToken($request->access_token)->first();
 		if (!$user) {
 			abort(404);
 		}

 		if ($user->api_token == '') {
 			$user->api_token = md5(microtime());
			$user->save();
 		}

 		$token = $user->api_token;
 		
 		return response()->json([
 				'token' => $token
 			]);
 		
 	}
}
