<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Event;
use App\EventAttendee;
use App\EventRequest;
use App\Services\EventService;
use DateTime;
use DB;
use Auth;

class EventsController extends Controller
{
	/**
	 * Get All events 
	 * @param  int $feed_id 
	 * @return json          
	 */
    public function getList(Request $request)
    {
       $query = Event::where('status','Published');
                    
        if ($request->start_date) {
            $query->where('start_time', '>=', $request->start_date);
        }

        if ($request->end_date) {
            $query->where('finish_time', '<=', $request->end_date);
        }

        $events = $query->orderBy('id','desc')->paginate(25);

        $result = [];
        foreach ($events as $event) {
            $result[] = [
                'event_id' => $event->id,
                'name' => $event->name,
                'location' => $event->location,
                'paid'      => $event->paid,
                'ticket_link'      => $event->ticket_link,
                'attendees' => DB::table('event_attendees')->groupBy('status')->lists(DB::raw('COUNT(*)'), 'status'),
                'cover_photo'      => $event->getCoverImageUrl(),
                'start_time' => $event->start_time,
                'finish_time' => $event->finish_time
            ];
        }

        return response()->json($result);
    }

    /**
     * Get event details
     * @return json 
     */
    public function getDetail($event_id)
    { 
        $event = Event::findOrFail($event_id);

        if ($event->status == 'Draft') {
            abort(404);
        }

        $event->attendees = ['attendees' => DB::table('event_attendees')->groupBy('status')->lists(DB::raw('COUNT(*)'), 'status')];

        return response()->json($event);
    }


    /**
     * Create or update a users attendance status
     * @return json 
     */
    public function postUpdateAttendance(Request $request, $event_id)
    {
        $this->validate($request, [
            'status' => 'required'
        ]);

        $attendee = EventAttendee::where('user_id', Auth::guard('api')->user()->id)->where('event_id', $event_id)->first();

        if(!$attendee) {
            $attendee = new EventAttendee;
            $attendee->user_id = Auth::guard('api')->user()->id;
            $attendee->event_id = $event_id;
        }

        $attendee->status = $request->status;

        $attendee->save();

        return response()->json('success');
    }


    /**
     * Event request from a user
     * @return json 
     */
    public function getRequestEvent(Request $request)
    {
        $this->validate($request, [
            'content' => 'required'
        ]);

        $event_request = new EventRequest;
        $event_request->user_id = Auth::guard('api')->user()->id;
        $event_request->content = $request->get('content','');

        $event_request->save();

        return response()->json([
                'message' => 'Requested added successfully.',
            ]);

    }
    
}
