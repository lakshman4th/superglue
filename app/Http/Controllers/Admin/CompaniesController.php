<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Company;
use App\CompanyStats;
use App\Account;
use App\Space;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Services\CompanyService;

class CompaniesController extends Controller
{
    /**
     * Show list of companies
     * @return view
     */
    public function getIndex()
    {
        $companies = Company::all();
        return view('pages.companies-list')
            ->with('companies',$companies)
            ->with('title', 'Companies List');
    }

    /**
     * Show create company page
     * @return view
     */
    public function getCreate()
    {
        $spaces = Space::lists('name','id');
        $accounts = Account::lists('name','id');
        return view('pages.companies-create')
            ->with('spaces',$spaces)
            ->with('accounts',$accounts);
    }

    /**
     * Get modal for create company
     * @return view
     */
    public function getCreateModal()
    {
        $spaces = Space::lists('name','id');
        $accounts = Account::lists('name','id');
        return view('pages.companies-create-form')
            ->with('spaces',$spaces)
            ->with('accounts',$accounts);
    }

    /**
     * Store company details
     * @param  Request $request
     * @return response
     */
    public function postStore(Request $request)
    {
        $this->validate($request, [
            'name'         => 'required',
            'account_id'   => 'required|exists:accounts,id',
            'space_id'     => 'required|exists:spaces,id',
            'industry'     => 'required',
            'abn'          => 'required',
            'date_started' => 'required|date',
            'employees'    => 'required|numeric',
            'investment'   => 'required',
            'revenue'      => 'required',
        ]);

        $companyService = new CompanyService;
        $company = $companyService->storeCompany($request);

        if ($request->ajax()) {
            return ['status'=>'success','company'=>$company];
        } else {
            return redirect('/companies');
        }

    }
}
