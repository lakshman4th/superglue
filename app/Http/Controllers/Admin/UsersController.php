<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Company;
use App\Account;
use App\Space;
use App\User;
use App\Timeline;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;
use Session;
use Auth;
use Carbon\Carbon;
use App\Services\UserService;
use App\Industry;

class UsersController extends Controller
{
	/**
	 * Show list of all Users with and without filters.
	 * @param  Request $request [description]
	 * @return [type] [description]
	 */
	public function getIndex(Request $request)
	{
		$users = User::filters($request);
		return view('pages.users-list')
				->with('users', $users)
				->with('title', 'User List');
	}

	/**

	 * Show create user form
	 * @return view
	 */
	public function getCreate()
	{
		$industries = Industry::industries();
		$spaces = Space::all();
		return view('pages.users-create')
			   ->with('industries',$industries)
			   ->with('spaces', $spaces);
	}

	/**
	 * Show user invite page
	 * @return view
	 */
	public function getInvite()
	{
		$accounts = Account::orderBy('name','asc')->get();

		return view('pages.users-invite')
				->with('accounts',$accounts)
				->with('title', 'Invite User');
	}

	/**
	 * Handle user invite data
	 * @param  Request $request
	 * @return redirect
	 */
	public function postInvite(Request $request)
	{
		$userService = new UserService;
		$userService->invite($request);

		return redirect('/admin/users');
	}

	/**
	 * Ajax call to resend invite email
	 * @param  Request $request
	 * @return null
	 */
	public function postResendInvite(Request $request)
	{
		$userService = new UserService;
		$userService->resendInvite($request);
	}

	/**
	 * Show Sign Up Page
	 * @return view
	 */
	public function getSignUp()
	{
		return view('pages.public.users-signup');
	}

	/**
	 * Show user profile page
	 * @param  int $user_id
	 * @return view
	 */
	public function getView($user_id)
	{
		$user = User::findOrFail($user_id);

		$title = $user->name . ' Profile';

		return view('pages.users-view')
				->with('user', $user)
				->with('title', $title);
	}

	/**
	 * Show edit user page
	 * @param  int $user_id
	 * @return view
	 */
	public function getEdit($user_id)
	{
		$user = User::findOrFail($user_id);
		$industries = Industry::industries();

		return view('pages.users-edit')
				->with('user', $user)
				->with('industries', $industries)
				->with('title', 'Edit User');
	}

	/**
	 * Handle edit user data
	 * @param  Request $request
	 * @param  int  $user_id
	 * @return redirect
	 */
	public function postEdit(Request $request,$user_id)
	{
		$this->validate($request,[
				'dob' => 'date_format:d/m/Y'
			]);
		$new_dob = $request->dob ? Carbon::createFromFormat('d/m/Y', $request->dob)->format('Y-m-d') : null;
		$request->merge(['dob' => $new_dob]);

		$userService = new UserService;
		$userService->edit($request,$user_id);

		return redirect('/admin/users');
	}

	/**
	 * Handle saving of user data
	 * @param  Request $request
	 * @return redirect
	 */
	public function postCreate(Request $request)
	{
		$this->validate($request,[
				'dob' => 'date_format:d/m/Y'
			]);

		$new_dob = $request->dob ? Carbon::createFromFormat('d/m/Y', $request->dob)->format('Y-m-d') : null;
		$request->merge(['dob' => $new_dob]);

		$userService = new UserService;
		$userService->create($request);

		return redirect('/admin/users');
	}


	public function getLoadTimeline(Request $request)
	{
		$result = ["status" => 1, 'message' => 'Feeds fetched.', 'feeds' => []];
		$offset = isset($request->offset)? $request->offset : 0;

		$feeds = Timeline::whereUserId($request->user_id)->limit(10)->offset($offset)->get();
		
		if (!count($feeds)) {
			$result = ["status" => 0, 'message' => 'No feeds found.'];
			return $result;
		}
		foreach ($feeds as $i => $timeline) {
			$result['feeds'][] = [
				'age' => $timeline->created_at->diffForHumans(),
				'user_name' => $timeline->user->first_name . ' ' . $timeline->user->last_name,
				'title' => $timeline->title,
				'author' => $timeline->author->first_name . ' ' . $timeline->author->last_name,
				'created_at' =>	$timeline->created_at->format('h:i a - d.m.Y'),
				'message' => $timeline->message
			];
		}
		return response()->json($result);
	}

	public function getTypeahead($phrase)
	{
		$users = User::whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", [$phrase . '%'])
		             ->orderBy('first_name')
		             ->orderBy('last_name')
		             ->take(10)
		             ->get(['id', 'account_id', 'first_name', 'last_name', 'email']);
		return response()->json($users);
	}
}
