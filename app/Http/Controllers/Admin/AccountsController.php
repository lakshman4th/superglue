<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Invoice;
use App\Account;
use App\User;
use App\Plan;
use App\Office;
use App\Desk;
use DateTime;
use DateInterval;
use Stripe;
use App\Space;
Use DB;
use App\Services\AccountService;
use Carbon\Carbon;

class AccountsController extends Controller
{
	public function getIndex()
	{
		return view('pages.accounts-list')
			 ->with('title', 'Accounts List')
			 ->with('accounts', Account::all());
	}

	public function getEdit(Request $request, $account_id)
	{
		
		$account = Account::find($account_id);

		return view('pages.accounts-edit')
		     ->with('request', $request)
		     ->with('account', $account);
	}

	public function postEdit(Request $request, $account_id)
	{
		$this->validate($request, [
			'name'               => 'required',
			'suburb'             => 'required_with:address',
			'state'              => 'required_with:address',
			'postcode'           => 'required_with:address|numeric',
			'country'            => 'required_with:address',
			'email'              => 'required|email'
		]);

		$account = Account::find($account_id);
		$account->name = $request->name;
		$account->address = $request->address;
		$account->suburb = $request->suburb;
		$account->state = $request->state;
		$account->postcode = $request->postcode;
		$account->country = $request->country;
		$account->abn = preg_replace('/[^\d]+/', '', $request->abn);
		$account->email = $request->email;
		$account->save();

		return redirect('/admin/accounts');
	}


	public function getView($account_id)
	{
		$account = Account::findOrFail($account_id);

		$invoices = DB::table('invoices AS i')
						->leftJoin('payments AS p', 'p.invoice_id', '=', 'i.id')
						->groupBy('i.id')
						->where('i.account_id',$account_id)
						->selectRaw("i.*, SUM(p.amount) AS amount_paid")
						->get();

		return view('pages.accounts-view')
			 ->with('title', 'View Account')
			 ->with('account', $account)
			 ->with('invoices', $invoices)
			 ->with('plans', Plan::all())
			 ->with('offices', Office::all())
			 ->with('desks', Desk::all());
	}

	public function postAssociateUser(Request $request, $account_id)
	{
		$this->validate($request, [
			'user_id' => 'required|exists:users,id',
		]);

		$user = User::find($request->user_id);

		if ($user->account_id) {
			return response()->json(['general' => ['This user is already in an account.']], 422);
		}

		$user->account_id = $account_id;
		$user->save();

		return response()->json(true);
	}

	public function postRemoveUser(Request $request, $account_id)
	{
		$this->validate($request, [
			'user_id' => 'required|exists:users,id',
		]);

		$user = User::find($request->user_id);

		if ($user->account_id != $account_id) {
			return response()->json(['general' => ['This user is not in this account.']], 422);
		}

		$user->account_id = null;
		$user->save();

		return response()->json(true);
	}

	public function getCreate()
	{
		$spaces = Space::orderBy('name')->get();
		return view('pages.accounts-create')
				->with('spaces',$spaces)
				->with('title', 'Create Account');
	}

	public function postDelete(Request $request,$account_id)
	{
		$this->validate($request,[
				'termination_date' => 'date_format:d/m/Y'
			]);

		$termination_date = $request->termination_date ? Carbon::createFromFormat('d/m/Y', $request->termination_date)->format('Y-m-d') : null;
		$request->merge(['termination_date' => $termination_date]);

		$service = new AccountService;
		$service->delete($request,$account_id);
	}

	public function postCreate(Request $request)
	{
		$this->validate($request, [
			'name'               => 'required',
			'suburb'             => 'required_with:address',
			'state'              => 'required_with:address',
			'postcode'           => 'required_with:address|numeric',
			'country'            => 'required_with:address',
			'email'              => 'required|email',
			'space'              => 'required',
		]);

		$account = new Account;
		$account->space_id = $request->space;
		$account->name = $request->name;
		$account->address = $request->address;
		$account->suburb = $request->suburb;
		$account->state = $request->state;
		$account->postcode = $request->postcode;
		$account->country = $request->country;
		$account->abn = preg_replace('/[^\d]+/', '', $request->abn);
		$account->email = $request->email;
		$account->save();

		return redirect('/admin/accounts/view/'.$account->id);
	}

	public function postCard(Request $request, $account_id)
	{
		$this->validate($request, [
			'card_token' => 'required',
		]);

		$account = Account::findOrFail($account_id);
		$account->setCard($request->card_token);
	}

	public function postAddCredit(Request $request,$account_id)
	{
		$service = new AccountService;
		$item = $service->purchaseCredit($request,$account_id);

	}
}
