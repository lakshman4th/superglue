<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\HolidayService;
use Carbon\Carbon;
use App\HolidayPeriod;

class HolidaysController extends Controller
{
	/**
	 * Ajax call to add holiday period to account
	 * @param  Request $request    
	 * @param  int  $account_id 
	 * @return null              
	 */
	public function postAdd(Request $request,$account_id)
	{

		$this->validate($request,[
				'start_date' => 'date_format:d/m/Y',
				'end_date' => 'date_format:d/m/Y',
			]);

		$request->merge([
				'start_date' => $request->start_date ? Carbon::createFromFormat('d/m/Y',$request->start_date)->format('Y-m-d') : null,
				'end_date' => $request->end_date ? Carbon::createFromFormat('d/m/Y',$request->end_date)->format('Y-m-d') : null,
			]);

		$service = new HolidayService;
		$service->add($request,$account_id);
	}

	/**
	 * Ajax call to update holiday period to account
	 * @param  Request $request    
	 * @param  int  $account_id 
	 * @return null              
	 */
	public function postEdit(Request $request,$holiday_id)
	{

		$this->validate($request,[
				'start_date' => 'date_format:d/m/Y',
				'end_date' => 'date_format:d/m/Y',
			]);

		$request->merge([
				'start_date' => $request->start_date ? Carbon::createFromFormat('d/m/Y',$request->start_date)->format('Y-m-d') : null,
				'end_date' => $request->end_date ? Carbon::createFromFormat('d/m/Y',$request->end_date)->format('Y-m-d') : null,
			]);

		$service = new HolidayService;
		$service->edit($request,$holiday_id);
	}

	public function postDelete(Request $request,$holiday_id)
	{
		HolidayPeriod::where('id',$holiday_id)->delete();
	}
}
