<?php
namespace App\Http\Controllers\Admin;

use App\Space;
use App\Desk;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Services\DeskService;
use Auth;


class DesksController extends Controller
{


	public function postCreate(Request $request)
	{

		$deskService = new DeskService;
		$deskService->create($request);

		return redirect('/admin/spaces/view/'.$request->space_id);
	}

	public function postEdit(Request $request, $desk_id)
	{
		$deskService = new DeskService;
		$desk = $deskService->edit($request,$desk_id);

		return redirect('/admin/spaces/view/'.$desk->space_id);
		
	}

	public function postDelete(Request $request,$desk_id)
	{
		Desk::where('id',$desk_id)->delete();
		return response()->json(true);
	}

	

}

