<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BillingItem;
use App\Plan;
use App\Office;
use App\Desk;
use Carbon\Carbon;

class BillingItemsController extends Controller
{

	public function postCreate(Request $request)
	{
		$this->validate($request, [
			'type'        => 'required',
			'name'        => 'required_if:type,other',
			'cost'        => 'numeric',
			'signup_fee'  => 'numeric',
			'num_credits' => 'numeric',
			'start_date'  => 'required|date_format:d/m/Y',
			'end_date'    => 'required_if:recurrence,limited|date_format:d/m/Y',
		]);

		$item = new BillingItem;
		$item->account_id = $request->account_id;

		$this->update($item, $request);

		if ($request->signup_fee > 0) {
			$this->addSignupFee($request);
		}
	}

	private function addSignupFee(Request $request)
	{
		$item = new BillingItem;
		$item->account_id = $request->account_id;
		$request->merge([
				'recurrence'  => 'none',
				'cost'		  => $request->signup_fee, 
				'num_credits' => 0,
			]);
		$this->update($item,$request);
	}

	public function postEdit(Request $request, $item_id)
	{
		$this->validate($request, [
			'type'        => 'required',
			'name'        => 'required_if:type,other',
			'cost'        => 'numeric',
			'num_credits' => 'numeric',
			'start_date'  => 'required|date_format:d/m/Y',
			'end_date'    => 'required_if:recurrence,limited|date_format:d/m/Y',
		]);

		$item = BillingItem::find($item_id);

		$this->update($item, $request);
	}

	private function update(BillingItem $item, Request $request)
	{
		$item->cost = $request->cost;
		$item->num_credits = $request->num_credits;
		$item->start_date = Carbon::createFromFormat('d/m/Y', $request->start_date);

		if ($request->type == 'other') {
			$entity_type = 'other';
		} else {
			list($entity_type, $entity_id) = explode(':', $request->type);
		}

		switch ($entity_type) {
			case 'plan':
				$item->plan_id = $entity_id;
				$item->name = 'Membership: ' . Plan::find($entity_id)->name;
				break;
			case 'office':
				$item->office_id = $entity_id;
				$item->name = 'Office: ' . Office::find($entity_id)->name;
				break;
			case 'desk':
				$item->desk_id = $entity_id;
				$item->name = 'Desk: ' . Desk::find($entity_id)->name;
				break;
			default:
				$item->name = $request->name;
		}

		switch ($request->recurrence) {
			case 'none':
				$item->end_date = $item->start_date;
				break;
			case 'indefinite':
				$item->end_date = null;
				break;
			case 'limited':
				$item->end_date = Carbon::createFromFormat('d/m/Y', $request->end_date);
		}

		$item->save();
	}

	public function postDelete($item_id)
	{
		BillingItem::whereId($item_id)->delete();
	}

}
