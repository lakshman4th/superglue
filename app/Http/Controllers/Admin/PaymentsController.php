<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\PaymentService;
use Carbon\Carbon;

class PaymentsController extends Controller
{
	/**
	 * Ajax call to add payment to invoice
	 * @param  Request $request 
	 * @return null           
	 */
	public function postAdd(Request $request)
	{
		$this->validate($request,[
				'payment_date' => 'date_format:d/m/Y',
			]);
		
		$payment_date = $request->payment_date ? Carbon::createFromFormat('d/m/Y', $request->payment_date)->format('Y-m-d') : null;
		$request->merge(['payment_date' => $payment_date]);

		$paymentService = new PaymentService;
		$paymentService->add($request);
	}

	/**
	 * Ajax call to edit payment details
	 * @param  Request $request    
	 * @param  int  $payment_id 
	 * @return null
	 */
	public function postEdit(Request $request,$payment_id)
	{
		$this->validate($request,[
				'payment_date' => 'date_format:d/m/Y',
			]);
		
		$payment_date = $request->payment_date ? Carbon::createFromFormat('d/m/Y', $request->payment_date)->format('Y-m-d') : null;
		$request->merge(['payment_date' => $payment_date]);

		$paymentService = new PaymentService;
		$paymentService->edit($request,$payment_id);
	}

	public function postDelete(Request $request,$payment_id)
	{
		$paymentService = new PaymentService;
		$paymentService->delete($payment_id);
	}
}
