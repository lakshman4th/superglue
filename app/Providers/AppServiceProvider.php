<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Event;
use DateTime;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		Event::listen('auth.login', function($user) {
			$user->last_login_at = new DateTime;
			$user->save();
		});
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
