<?php

return array(

    'appNameIOS'     => array(
        'environment' =>env('IOS_ENVIRONMENT', 'development'),
        'certificate' =>env('IOS_CERTIFICATE', '/path/to/certificate.pem'),
        'passPhrase'  =>env('IOS_PASSPHRASE', 'password'),
        'service'     =>env('IOS_SERVICE', 'apns')
    ),
    'appNameAndroid' => array(
        'environment' =>env('ANDROID_ENVIRONMENT', 'production'),
        'apiKey'      =>env('ANDROID_APIKEY', 'yourAPIKey'),
        'service'     =>env('ANDROID_SERVICE', 'gcm')
    )
);