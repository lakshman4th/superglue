@extends('layouts.default')
@section('content')
    <div class="wrapper wrapper-content  animated fadeInRight">
        <div class="row">
            
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>{{ $title or '' }}</h5>

                        <div class="ibox-tools">
                            <a href="/admin/powerups/edit/{{ $powerup->id }}" class="btn btn-default btn-xs">Edit Powerup</a>
                        </div>
                       
                    </div>
                    <div class="ibox-content">
                        <div class="form-horizontal">
                                                            
                            <div class="form-group">    
                                <label class="col-lg-2 control-label">Title :</label>
                                <div class="col-lg-4">
                                    <p class="form-control-static"> {{ $powerup->title }}</p>
                                </div>
                            </div>
                            <div class="form-group">    
                                <label class="col-lg-2 control-label">Class :</label>
                                <div class="col-lg-4">
                                    <p class="form-control-static"> {{ $powerup->class }}</p>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-lg-2 control-label">Description :</label>
                                <div class="col-lg-4">
                                    <p class="form-control-static"> {{ $powerup->description }}</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">Link :</label>
                                <div class="col-lg-4">
                                    <p class="form-control-static"> {{ $powerup->link }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Type :</label>
                                <div class="col-lg-4">
                                    <p class="form-control-static"> {{ $powerup->type }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Gender :</label>
                                <div class="col-lg-4">
                                    <p class="form-control-static"> {{ $powerup->gender }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Certify :</label>
                                <div class="col-lg-4">
                                    <p class="form-control-static"> {{ ($powerup->certify)?'Yes':'No' }}</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    
@endsection