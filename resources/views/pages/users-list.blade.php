@extends('layouts.default')
@section('content')
	<div class="wrapper wrapper-content  animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>{{ $title or '' }}</h5>
						<div class="ibox-tools">
							<a href="/admin/users/create" class="btn btn-primary btn-xs">Create User</a>
							<a href="/admin/users/invite" class="btn btn-primary btn-xs">Invite</a>
						</div>
					</div>
					<div class="ibox-content">
						<div class="row">
							<form method="GET" action="/admin/users">
								<div class="col-sm-11">
									<div class="input-group">
										<input type="text" placeholder="Search Users" class="input form-control" name="global_search" value="{{Request::get('global_search')}}">
										<span class="input-group-btn">
											<button type="submit" class="btn btn btn-primary "> <i class="fa fa-search"></i> Search</button>
										</span>
									</div>
								</div>
								<select name="user_type" id="user_type" style="height:30px" onchange="this.form.submit()">
									<option {{Request::get('user_type') == 'All' ? 'selected' : '' }} >All</option>
									<option {{Request::get('user_type') == 'Admin' ? 'selected' : '' }} >Admin</option>
									<option {{Request::get('user_type') == 'Mentor' ? 'selected' : '' }} >Mentor</option>
									<option {{Request::get('user_type') == 'Member' ? 'selected' : '' }} >Member</option>
								</select>
							</form>
						</div>

						<table class="table table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Type</th>
									<th>Created At</th>
									<th>Last Login</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody class="table_body">
								@if(count($users)!==0)
									@foreach($users as $user)
										<tr>
											<td>{{ $user->first_name . ' ' . $user->last_name }}</td>
											<td>{{ $user->email }}</td>
											<td>{{ ucfirst($user->type) }}</td>
											<td>{{ $user->created_at->format('d M Y') }}</td>
											<td>{{ $user->last_login_at }}</td>
											<td>{{ $user->invitations->first() ? 'Pending' : 'Active' }}</td>
											<td>
												<div class="btn-group">
													<button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
													<ul class="dropdown-menu">
														<li><a href="/admin/users/view/{{$user->id}}">View</a></li>
														<li><a href="/admin/users/edit/{{$user->id}}">Edit</a></li>
														@if ($user->invitations->first())
														<li>
															<a href="#" onclick="resend_invite('{{$user->id}}',this);return false;">Resend Invite <br><small>{{ 'Last Sent: ' . $user->invitations->first()->updated_at->format('F d, Y')}}</small></a>
														</li>
														@endif
														<li class="divider"></li>
														<li><a href="#">Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
									@endforeach
								@else
								    <tr>
								    	<td colspan="7" class="text-center">No user found</td>
								    </tr>
								@endif	
								@if ($users->total() > 25)
									<tr>
										<td colspan="7" align="right">
											{{$users->render()}}
										</td>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script>
		
		function resend_invite(user_id,selector)
		{
			if ($(selector).hasClass('disabled')) {
				return false;
			}
			$(selector).addClass('disabled');
			$(selector).text('Sending invite...');
			$.ajax({
				url: '/admin/users/resend-invite',
				type: "post",
				data:
				{
					'_token': '{{csrf_token()}}',
					'user_id' : user_id,
				},
				success: function(){
					$(selector).removeClass('disabled');
					$(selector).text('Invite Sent');
				}
			});
		}
		
	</script>
@endsection