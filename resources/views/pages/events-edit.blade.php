@extends('layouts.default')
@section('content')
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>{{ $title or '' }}</h5>
					
				</div>
				<div class="ibox-content">
					@include('partials.errors')

					<form class="form-horizontal" method="post" action="/admin/events/edit/{{$event->id}}" enctype="multipart/form-data">
						{{csrf_field()}}
						<div class="form-group">
							<label class="col-lg-2 control-label">Name</label>
								<div class="col-lg-10"><input type="text" placeholder="Event Title" name="name" class="form-control" value="{{ old('name',$event->name) }}">
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Description</label>
								<div class="col-lg-10">
								<textarea  placeholder="Description" name="description" class="form-control" >{{ old('description',$event->description) }}</textarea>
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Location</label>
								<div class="col-lg-10"><input type="text" placeholder="Location" name="location" class="form-control" value="{{ old('location',$event->location) }}">
								</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Paid</label>
								<div class="col-lg-10">
								<select class='form-control' placeholder='Parent' name="paid">
										<option value="">Please select</option>
										<option value="1" {{ old('paid',$event->paid) ==1 ? 'selected' :''}}>Yes</option>
										<option value="0" {{ old('paid',$event->paid) ==0 ? 'selected' :''}}>No</option>
								</select>
								</div>
						</div>

						<div class="form-group">
								<label class="col-lg-2 control-label">Start Date/Time</label>
								<div class="col-lg-10">
									<input type="text" name="start_time" class="date-input form-control" value="{{ old('start_time',$event->start_time) ?  Carbon\Carbon::createFromFormat('Y-m-d H:i:s',old('start_time',$event->start_time))->format('d/m/Y H:i:s') : '' }}">
								</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Finish Date/Time</label>
							<div class="col-lg-10">
								<input type="text" name="finish_time" class="date-input form-control" value="{{ old('start_time',$event->finish_time) ?  Carbon\Carbon::createFromFormat('Y-m-d H:i:s',old('finish_time',$event->finish_time))->format('d/m/Y H:i:s') : '' }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Ticket link</label>
								<div class="col-lg-10"><input type="text" placeholder="Ticket link" name="ticket_link" class="form-control" value="{{ old('ticket_link',$event->ticket_link) }}">
								</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Status</label>
								<div class="col-lg-10">
								<select name="status" class="form-control">
									<option value="">Select Status</option>
									@foreach (['Draft','Published'] as $status)
									<option {{old('status',$event->status) == $status ? 'selected' : '' }}>{{$status}}</option>
									@endforeach
								</select>
								</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">Cover Photo</label>
							<div class="col-lg-10">
								<input type="file" name="cover_photo" accept="image/jpeg" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button class="btn btn-sm btn-white" type="submit">Update</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	<script>
		$(document).ready(function(){
			$('.date-input').datetimepicker({
				format : 'DD/MM/YYYY H:mm:ss',
				useCurrent : false,
			});
		});
	</script>

@endsection

