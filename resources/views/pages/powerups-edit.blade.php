@extends('layouts.default')
@section('content')
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>{{ $title or '' }}</h5>
					
				</div>
				<div class="ibox-content">
					@include('partials.errors')

					<form class="form-horizontal" method="post" action="/admin/powerups/edit/{{$powerup->id}}">
						{{csrf_field()}}
						<div class="form-group">
							<label class="col-lg-2 control-label">Title</label>
								<div class="col-lg-10">
								<textarea  placeholder="Powerup Title" name="title" class="form-control" >{{ old('title',$powerup->title) }}</textarea>
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Class</label>
								<div class="col-lg-10">
								<textarea  placeholder="Powerup Class" name="class" class="form-control" >{{ old('class',$powerup->class) }}</textarea>
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Description</label>
								<div class="col-lg-10"><input type="text" placeholder="Description" name="description" class="form-control" value="{{ old('content',$powerup->description) }}">
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Link</label>
								<div class="col-lg-10"><input type="text" placeholder="Link" name="link" class="form-control" value="{{ old('link',$powerup->link) }}">
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Type</label>
								<div class="col-lg-10">
									<select name="type" class='form-control'  id="type">
										<option value="Business" {{ old('type',$powerup->type) == 'Business' ? 'selected':''}}>Business</option>
										<option value="Learning" {{ old('type',$powerup->type) == 'Learning' ? 'selected':''}}>Learning</option>
										<option value="Buyyer" {{ old('type',$powerup->type) == 'Buyyer' ? 'selected':''}}>Buyyer</option>
										<option value="Seller" {{ old('type',$powerup->type) == 'Seller' ? 'selected':''}}>Seller</option>
									</select>
						        </div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Gender</label>
								<div class="col-lg-2">
									<input type="radio"  name="gender"  value="Male" {{ old('gender',$powerup->gender) =='Male'?'checked':'' }} >
									<label control-label">Male</label>
								</div>
								<div class="col-lg-2">
									<input type="radio"  name="gender"  value="Female" {{ old('gender',$powerup->gender) == 'Female'?'checked':'' }} >
									<label  control-label">Female</label>
								</div>
						</div>   
						<div class="form-group">
							<label class="col-lg-2 control-label">Certify</label>
								<div class="col-lg-10"><input type="checkbox" name="certify" value="1" {{ old('certify',$powerup->certify) =='1' ?'checked':''}} >
								</div>
						</div>      

						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button class="btn btn-sm btn-white" type="submit">Update</button>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	<script>
		$(document).ready(function(){
			$('.date-input').datetimepicker({
				format : 'DD/MM/YYYY H:mm:ss',
				useCurrent : false,
			});
		});
	</script>

@endsection

