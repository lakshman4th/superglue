@extends('layouts.default')
@section('content')
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>{{ $title or '' }}</h5>
					
				</div>
				<div class="ibox-content">
					@include('partials.errors')

					<form class="form-horizontal" method="post" action="/admin/powerups/create">
						{{csrf_field()}}
						<div class="form-group">
							<label class="col-lg-2 control-label">Title</label>
								<div class="col-lg-10"><input type="text" placeholder="Powerup Title" name="title" class="form-control" value="{{ old('title') }}">
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Class</label>
								<div class="col-lg-10"><input type="text" placeholder="Powerup Class" name="class" class="form-control" value="{{ old('class') }}">
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Description</label>
								<div class="col-lg-10">
								<textarea  placeholder="Description" name="description" class="form-control" >{{ old('description') }}</textarea>
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Link</label>
								<div class="col-lg-10"><input type="text" placeholder="Link" name="link" class="form-control" value="{{ old('link') }}">
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Type</label>
								<div class="col-lg-10">
									<select name="type" class='form-control'  id="type">
										<option disabled selected="selected" >Select Type</option>
										<option value="Business" {{ old('type') == 'Business' ? 'selected':''}}>Business</option>
										<option value="Learning" {{ old('type') == 'Learning' ? 'selected':''}}>Learning</option>
										<option value="Buyyer" {{ old('type') == 'Buyyer' ? 'selected':''}}>Buyyer</option>
										<option value="Seller" {{ old('type') == 'Seller' ? 'selected':''}}>Seller</option>
									</select>
						        </div>
						</div> 
						<div class="form-group">
							<label class="col-lg-2 control-label">Gender</label>
								<div class="col-lg-2">
									<input type="radio"  name="gender"  value="Male" {{ old('gender') =='Male'?'checked':'' }} >
									<label class="control-label">Male</label>
								</div>
								<div class="col-lg-2">
									<input type="radio"  name="gender"  value="Female" {{ old('gender') == 'Female'?'checked':'' }} >
									<label  class="control-label">Female</label>
								</div>
						</div>   
						<div class="form-group">
							<label class="col-lg-2 control-label">Certify</label>
								<div class="col-lg-10"><input type="checkbox" name="certify" value="1" {{ old('certify') =='1' ?'checked':''}}>
								</div>
						</div>    
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button class="btn btn-sm btn-white" type="submit" id="create">Create</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	<script>
		$(document).ready(function(){
			$('.date-input').datetimepicker({
				format : 'DD/MM/YYYY H:mm:ss',
				useCurrent : false,
			});
		});
	</script>
<!-- 	<script>
		$('#create').on('click', function(event) {
			event.preventDefault();
			var  titel = $(this).closest('tr').data('id');
			bootbox.confirm('Are you sure you want to delete this work history?', function(response){
				if (response) {
					$.ajax({
						type: "post",
						url: "/work-history/delete/" + workhistory_id,
						data: {_token: '{{ csrf_token() }}', workhistory_id: workhistory_id},
					}).done(function(response) {
						if (response) {
							window.location.reload();
						}
					});
				}
			});
		});
	</script> -->

@endsection

