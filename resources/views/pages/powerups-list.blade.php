@extends('layouts.default')

@section('content')
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>{{ $title or '' }}</h5>
						<div class="ibox-tools">
							<a href="/admin/powerups/create" class="btn btn-primary btn-xs">Create New Powerup</a>
						</div>
						<div class="ibox-tools">
							<a href="#" id='create' class="btn btn-primary btn-xs create">Demo Test New Powerup</a>
						</div>
					</div>
					<div class="ibox-content">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Title</th>
									<th>Class</th>
									<th>Description</th>
									<th>Link</th>
									<th>Type</th>							
									<th>Gender</th>							
									<th>Certify</th>									
								</tr>
							</thead>
							<tbody>
								@if(count($powerups)!==0)
								@foreach ($powerups as $powerup)
									<tr data-id="{{$powerup->id}}"
									  data-title="{{$powerup->title}}" data-class="{{$powerup->class}}"
									  data-description="{{$powerup->description}}"
									  data-link="{{$powerup->link}}"
									  data-type="{{$powerup->type}}"
									  data-gender="{{$powerup->gender}}"
									  data-certify="{{$powerup->certify}}">
										<td>{{ $powerup->title }}</td>
										<td>{{ $powerup->class }}</td>
										<td>{{ $powerup->description }}</td>
										<td>{{ $powerup->link }}</td>
										<td>{{ $powerup->type }}</td>
										<td>{{ $powerup->gender }}</td>
										<td>{{ ($powerup->certify)?'Yes':'No' }}</td>
										<td>
											<div class="btn-group">
													<button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
													<ul class="dropdown-menu">
														<li><a href="#" class="viwbt">View</a></li>
														<li><a href="#" class="btn-edit-powerups">Edit</a></li>
														<li class="divider"></li>
														<li><a href="/admin/powerups/delete/{{$powerup->id}}">Delete</a></li>
													</ul>
												</div>
										</td>
									</tr>
								@endforeach
								@else
									<tr>
										<td colspan="6">No powerups</td>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
						
@endsection
@section('scripts')
<script src="/js/modalform.js"></script>
<script>
	var powr_up_model_html = ''+
	        '<form class="form-horizontal" id="creatform" method="post" action="/admin/powerups/create">'+
					'{{csrf_field()}}'+
						'<div class="form-group">'+
								'<label class="col-lg-2 control-label">Title</label>'+
									'<div class="col-lg-10"><input type="text" placeholder="Powerup Title" name="title" class="form-control" value="{{ old('title') }}" >'+
							         '</div>'+
							'</div>'+
							'<div class="form-group">'+
								'<label class="col-lg-2 control-label">Class</label>'+
									'<div class="col-lg-10"><input type="text" placeholder="Powerup Class" name="Class" class="form-control" value="{{ old('Class') }}" ></div>'+
							'</div>'+
							'<div class="form-group"><label class="col-lg-2 control-label">Description</label><div class="col-lg-10"><textarea  placeholder="Description" name="description" class="form-control" >{{ old('description') }}</textarea></div>'+
							'</div>'+
							'<div class="form-group"><label class="col-lg-2 control-label">Link</label><div class="col-lg-10"><input type="text" placeholder="Link" name="link" class="form-control" value="{{ old('link') }}"></div>'+
							'</div>'+
							'<div class="form-group"><label class="col-lg-2 control-label">Type</label>'+
							'<div class="col-lg-10"><select name="type" class="form-control"  id="type"><option disabled selected="selected" >Select Type</option><option value="Business" {{ old('type') == 'Business' ? 'selected':''}}>Business</option><option value="Learning" {{ old('type') == 'Learning' ? 'selected':''}}>Learning</option><option value="Buyyer" {{ old('type') == 'Buyyer' ? 'selected':''}}>Buyyer</option><option value="Seller" {{ old('type') == 'Seller' ? 'selected':''}}>Seller</option></select></div>'+
							'</div>'+ 
							'<div class="form-group">'+
							'<label class="col-lg-2 control-label">Gender</label>'+
								'<div class="col-lg-2">'+
									'<label class="control-label">Male <input type="radio"  name="gender"  value="Male" {{ old('gender') =='Male'?'checked':'' }} ></label></div>'+
								'<div class="col-lg-2">'+
									'<label class="control-label">Female <input type="radio"  name="gender"  value="Female" {{ old('gender') == 'Female'?'checked':'' }} ></label>'+
								'</div>'+
						'</div>'+
							'<div class="form-group"><label class="col-lg-2 control-label">Certify</label>'+
						'<div class="col-lg-10"><input type="checkbox" name="certify"  value="1" {{ old('certify') =='1' ?'checked':''}} ></div>'+'</div>'+
						'</form>';

$('.create').on('click', function(event) {
	event.preventDefault();
	modalform.dialog({
		bootbox: {
			title: 'Create New powerups',
			message: powr_up_model_html,
			buttons: {
				cancel: {
					label: 'Cancel',
					className: 'btn-default'
				},
				submit: {
					label: 'Create Powerup',
					className: 'btn-primary'
				}
			}
		}
	});
});
	var powr_up_edit_model_html = ''+
	        '<form class="form-horizontal" id="creatform" method="post" action="/admin/powerups/edit">'+
					'{{csrf_field()}}'+
						'<div class="form-group">'+
								'<label class="col-lg-2 control-label">Title</label>'+
									'<div class="col-lg-10"><input type="text" placeholder="Powerup Title" name="title" class="form-control" value="{{ old('title') }}" >'+
							         '</div>'+
							'</div>'+
							'<div class="form-group">'+
								'<label class="col-lg-2 control-label">Class</label>'+
									'<div class="col-lg-10"><input type="text" placeholder="Powerup Class" name="Class" class="form-control" value="{{ old('Class') }}" ></div>'+
							'</div>'+
							'<div class="form-group"><label class="col-lg-2 control-label">Description</label><div class="col-lg-10"><textarea  placeholder="Description" id="description" name="description" class="form-control" >{{ old('description') }}</textarea></div>'+
							'</div>'+
							'<div class="form-group"><label class="col-lg-2 control-label">Link</label><div class="col-lg-10"><input type="text" placeholder="Link" name="link" class="form-control" value="{{ old('link') }}"></div>'+
							'</div>'+
							'<div class="form-group"><label class="col-lg-2 control-label">Type</label>'+
							'<div class="col-lg-10"><select name="type" class="form-control"  id="type"><option disabled selected="selected" >Select Type</option><option value="Business" {{ old('type') == 'Business' ? 'selected':''}}>Business</option><option value="Learning" {{ old('type') == 'Learning' ? 'selected':''}}>Learning</option><option value="Buyyer" {{ old('type') == 'Buyyer' ? 'selected':''}}>Buyyer</option><option value="Seller" {{ old('type') == 'Seller' ? 'selected':''}}>Seller</option></select></div>'+
							'</div>'+ 
							'<div class="form-group">'+
							'<label class="col-lg-2 control-label">Gender</label>'+
								'<div class="col-lg-2">'+
									'<label class="control-label">Male <input type="radio"  name="gender"  value="Male" {{ old('gender') =='Male'?'checked':'' }} ></label></div>'+
								'<div class="col-lg-2">'+
									'<label class="control-label">Female <input type="radio"  name="gender"  value="Female" {{ old('gender') == 'Female'?'checked':'' }} ></label>'+
								'</div>'+
						'</div>'+
							'<div class="form-group"><label class="col-lg-2 control-label">Certify</label>'+
						'<div class="col-lg-10"><input type="checkbox" name="certify" id=="check" value="1"  ></div>'+'</div>'+
						'</form>';
	$('.btn-edit-powerups').on('click', function(event) {
		event.preventDefault();
		var tr = $(this).closest('tr');
		modalform.dialog({
			bootbox: {
				title: 'Edit Pwwerups',
				message: powr_up_edit_model_html,
				buttons: {
					cancel: {
						label: 'Cancel',
						className: 'btn-default',
					},
					submit: {
						label: 'Save Changes',
						className: 'btn-primary'
					}
				}
			},
			after_init: function() {
				$('.modal input[name="title"]').val(tr.data('title'));
				$('.modal input[name="Class"]').val(tr.data('class'));
				$('.modal #description').text(tr.data('description'));
				$('.modal input[name="link"]').val(tr.data('link'));
				var type = tr.data('type');
                var gender = tr.data('gender');
                
                (gender == 'Male')? $('[name="gender"][value="Male"]input:radio').click(): $('[name="gender"][value="Female"]input:radio').click();
				var check = tr.data('certify');
				
				(check == 1)?$('input[name="certify"]').attr('checked','true'):'';
				if(type == 'Business')
				{
					$('#type').val("Business");
				}
				else if(type == 'Buyyer')
				{
					$('#type').val("Buyyer");
				}
				else if(type == 'Learning')
				{
					$('#type').val("Learning");
				}
				else if(type == 'Seller')
				{
					$('#type').val("Seller");
				}
				$('.modal form').attr('action', '/admin/powerups/edit/' + tr.data('id'));
			}
		});
	});

var view_html =   '<form class="form-horizontal" id="creatform" method="post" action="/admin/powerups/create">'+
							'<div class="form-horizontal">'+
                                                            
                           ' <div class="form-group"> '+   
                               ' <label class="col-lg-3 control-label">Title :</label>'+
                                '<div class="col-lg-4"><p class="form-control-static-title"  id="title"></p></div>'+
                            '</div>'+
                            '<div class="form-group"><label class="col-lg-3 control-label">Class :</label><div class="col-lg-4"><p class="form-control-static" id="class"></p></div></div>'+
                            '<div class="form-group"><label class="col-lg-3 control-label">Description :</label><div class="col-lg-4"><p class="form-control-static" id="description"> </p></div></div>'+
                            '<div class="form-group"><label class="col-lg-3 control-label">Link :</label> <div class="col-lg-4"><p class="form-control-static" id="link"> </p></div></div>'+
                            '<div class="form-group"><label class="col-lg-3 control-label">Type :</label><div class="col-lg-4"><p class="form-control-static" id="type"> </p></div></div>'+
                            '<div class="form-group"><label class="col-lg-3 control-label">Gender :</label><div class="col-lg-4"><p class="form-control-static" id="gender"> </p> </div> </div>'+
                            '<div class="form-group"><label class="col-lg-3 control-label">Certify :</label><div class="col-lg-4"> <p class="form-control-static" id="certify"></p></div> </div>'+

                        '</div>'+
                        '</form>';;

$('.viwbt').on('click', function(event) {
	event.preventDefault();
    var tr = $(this).closest('tr');
	modalform.dialog({
		bootbox: {
			title: 'View powerups',
			message: view_html,
			buttons: {
				cancel: {
				label: 'Cancel',
				className: 'btn-default',
				},
			submit: {
					label: 'Save Changes',
					className: 'btn-primary',
					display:'none'
					}
			}
		},
		after_init: function() {
				$('.modal #title').text(tr.data('title'));
				$('.modal #class').text(tr.data('class'));
				$('.modal #description').text(tr.data('description'));
				$('.modal #link').text(tr.data('link'));
				$('.modal #type').text(tr.data('type'));
				$('.modal #gender').text(tr.data('gender'));
				var check = tr.data('certify');
				
				(check == 1)?$('#certify').text('Yes'):$('#certify').text('No');
			}
	})
});                        

</script>
@endsection
