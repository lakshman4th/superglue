@extends('layouts.default')
@section('content')
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>{{ $title or '' }}</h5>
					
				</div>
				<div class="ibox-content">
					@include('partials.errors')

					<form class="form-horizontal" method="post" action="/admin/announcements/create">
						{{csrf_field()}}
						<div class="form-group">
							<label class="col-lg-2 control-label">Title</label>
								<div class="col-lg-10"><input type="text" placeholder="Announcement Title" name="title" class="form-control" value="{{ old('title') }}">
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Content</label>
								<div class="col-lg-10">
								<textarea  placeholder="Content" name="content" class="form-control" >{{ old('content') }}</textarea>
								</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">Link</label>
								<div class="col-lg-10"><input type="text" placeholder="Link" name="link" class="form-control" value="{{ old('link') }}">
								</div>
						</div>

						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button class="btn btn-sm btn-white" type="submit">Create</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	<script>
		$(document).ready(function(){
			$('.date-input').datetimepicker({
				format : 'DD/MM/YYYY H:mm:ss',
				useCurrent : false,
			});
		});
	</script>

@endsection

