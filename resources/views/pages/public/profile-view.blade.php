@extends('layouts.default')
@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row m-b-lg m-t-lg">
		<div class="col-md-6">

			<div class="profile-image">
				<img src="{{ $user->avatar !== '' ? $user->avatarUrl : 'http://placehold.it/100x100' }}" class="img-circle circle-border m-b-md" alt="profile">
			</div>
			<div class="profile-info">
				<div class="">
					<div>
						<h2 class="no-margins">
							{{ $user->first_name . ' ' . $user->last_name }}
						</h2>
						<h4>{{ $user->job_title or '' }}</h4>
						<p> Company: {{ $user->company_name or '' }}</p>
						<p> Industry: {{ $user->industry or '' }}</p>
						<small>
							{{ $user->bio }}
						</small>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="row">

		<div class="col-lg-3">

			<div class="ibox">
				<div class="ibox-content">
						<h3>About <b>{{ Auth::user()->first_name }}</b></h3>

					<p class="small">
						{{ $user->bio }}
					</p>
				</div>
			</div>

			<div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Bookings</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="feed-activity-list">

                        <div class="feed-element">
                            <div>
                                <small class="pull-right text-navy">5 days time</small>
                                <strong>Monica Smith</strong>
                                <div>LT2 Substation - Board Room - 2 hours</div>
                                <small class="text-muted">5:00 pm - 12.06.2016</small>
                                <div class="actions">
                                    <a class="btn btn-xs btn-white"><i class="fa fa-cog"></i> Edit </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
		</div>

	</div>

</div>
@endsection
