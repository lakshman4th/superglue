@extends('layouts.default')
@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row m-b-lg m-t-lg">
		<div class="col-md-6">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Billing Items</h5>
					<div class="ibox-tools">
						<a href="#" class="btn btn-primary btn-xs" onclick="add_billing_item();return false;">Add</a>
					</div>
				</div>
				<div class="ibox-content">
					<table class="table">
						<thead>
							<tr>
								<th>Plan</th>
								<th>Office</th>
								<th>Desk</th>
								<th>Name</th>
								<th>Cost</th>
								<th>Credits</th>
								<th>Start Date</th>
								<th>End Date</th>
							</tr>
						</thead>
						<tbody>
							@if (count($billing_items))
							@foreach ($billing_items as $item)
							<tr>
								<td>{{ $item->plan ? $item->plan->name : '-' }}</td>
								<td>{{ $item->office ? $item->office->name : '-' }}</td>
								<td>{{ $item->desk ? $item->desk->name : '-' }}</td>
								<td>{{ $item->name }}</td>
								<td>${{ number_format($item->cost,2) }}</td>
								<td>{{ $item->num_credits }}</td>
								<td>{{ $item->start_date ? Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$item->start_date)->format('F d, Y') : '-' }}</td>
								<td>{{ $item->end_date ? Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$item->end_date)->format('F d, Y') : '-' }}</td>
							</tr>
							@endforeach
							@else
							<tr>
								<td align="center" colspan="8">No billing items.</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Invoices</h5>
				</div>
				<div class="ibox-content">
					<table class="table">
						<thead>
							<tr>
								<th>Invoice Number</th>
								<th>Total</th>
								<th>Amount Paid</th>
								<th>Status</th>
								<th>Due Date</th>
							</tr>
						</thead>
						<tbody>
							@if (count($invoices))
							@foreach ($invoices as $invoice)
								<tr>
									<td>{{ sprintf('INV-%05d', $invoice->id) }}</td>
									<td>${{ number_format($invoice->total, 2) }}</td>
									<td>${{ number_format($invoice->amount_paid, 2) }}</td>
									<td>{{ ucfirst($invoice->status) }}</td>
									<td>{{ Carbon\Carbon::createFromFormat('Y-m-d',$invoice->due_date)->format('j/m/Y') }}</td>
								</tr>
							@endforeach
							@else
								<tr>
									<td align="center" colspan="5">No Invoices</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-b-lg m-t-lg">
		<div class="col-md-6">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Payments</h5>
				</div>
				<div class="ibox-content">
					<table class="table">
						<thead>
							<tr>
								<th>Invoice ID</th>
								<th>Stripe ID</th>
								<th>Amount</th>
								<th>Method</th>
								<th>Payment Date</th>
							</tr>
						</thead>
						<tbody>
							@if (count($payments))
								@foreach ($payments as $payment)
								<tr>
									<td>{{ sprintf('INV-%05d', $payment->invoice_id) }}</td>
									<td>{{ $payment->stripe_transaction_id }}</td>
									<td>${{ number_format($payment->amount,2) }}</td>
									<td>{{ ucwords($payment->method) }}</td>
									<td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$payment->payment_date)->format('d/m/Y') }}</td>
								</tr>
								@endforeach
							@else
								<tr>
									<td colspan="5" align="center">No payments</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Credit History</h5>
				</div>
				<div class="ibox-content">
					<table class="table">
						<thead>
							<tr>
								<th>Description</th>
								<th>Amount</th>
								<th>Created At</th>
						</thead>
						<tbody>
							@if (count($credit_transactions))
							@foreach ($credit_transactions as $transaction)
								<tr>
									<td>{{ $transaction->description }}</td>
									<td>${{ number_format($transaction->amount, 2) }}</td>
									<td>{{ $transaction->created_at->format('F d, Y') }}</td>
								</tr>
							@endforeach
							@else
								<tr>
									<td align="center" colspan="3">No Transactions</td>
								</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row m-b-lg m-t-lg">
		<div class="col-lg-6">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Credit Card Details</h5>
				</div>
			<!-- 	<div class="ibox-content">
					@if ($user->account->card_last_four)
						<p>{{ $user->account->card_brand }}: xxxx xxxx xxxx {{ $user->account->card_last_four }} &nbsp;<button type="button" class="btn btn-default btn-configure-card">Edit</button></p>
					@else
						<p><em class="text-muted">Not configured</em> &nbsp; <button type="button" class="btn btn-default btn-configure-card">Configure</button></p>
					@endif
				</div>
			</div> -->
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script src="/js/modalform.js"></script>
	<script src="https://js.stripe.com/v2/"></script>
	<script>
		Stripe.setPublishableKey('{{ env('STRIPE_KEY') }}');
		$(document).ready(function(){
			$('.btn-configure-card').on('click', function(event) {
				event.preventDefault();

				bootbox.dialog({
					title: 'Configure Credit Card',
					message: '' +
						'<div class="form-horizontal">' +
							'<div class="form-group">' +
								'<label class="col-md-4 control-label">Card Number</label>' +
								'<div class="col-md-8"><input type="text" name="card_number" class="form-control"></div>' +
							'</div>' +
							'<div class="form-group">' +
								'<label class="col-md-4 control-label">Expiry</label>' +
								'<div class="col-md-4">' +
									'<select name="card_expiry_month" class="form-control">' +
										@for ($i = 1; $i <= 12; $i++)
											'<option value="{{ $i }}">{{ sprintf('%02d', $i) }} ({{ (new DateTime("2000-$i-15"))->format('F') }})</option>' +
										@endfor
									'</select>' +
								'</div>' +
								'<div class="col-md-4">' +
									'<select name="card_expiry_year" class="form-control">' +
										@for ($i = date('Y'); $i <= date('Y') + 15; $i++)
											'<option value="{{ $i }}">{{ $i }}</option>' +
										@endfor
									'</select>' +
								'</div>' +
							'</div>' +
							'<div class="form-group">' +
								'<label class="col-md-4 control-label">CVC</label>' +
								'<div class="col-md-8"><input type="text" name="card_cvc" class="form-control"></div>' +
							'</div>' +
						'</div>',
					buttons: {
						submit: {
							label: 'Save Changes',
							className: 'btn-primary',
							callback: function() {
								$('.modal-footer .text-danger').remove();
								$('.modal-footer button').attr('disabled','disabled');

								// Send the CC details to Stripe to get a card token back
								Stripe.card.createToken({
									number: $('input[name="card_number"]').val(),
									exp_month: $('select[name="card_expiry_month"]').val(),
									exp_year: $('select[name="card_expiry_year"]').val(),
									cvc: $('input[name="card_cvc"]').val()
								}, function(status, response) {
									if (response.error) {
										$('.modal-footer').prepend($('<span class="text-danger pull-left"></span>').html(response.error.message));
										$('.modal-footer button').removeAttr('disabled');
										return;
									}

									// Submit AJAX request to our server
									$.ajax({
										url: '/account/card/{{ $user->account->id }}',
										method: 'post',
										data: {
											card_token: response.id,
											_token: '{{ csrf_token() }}'
										},
										success: function() {
											bootbox.hideAll();
											document.location.reload();
										},
										error: function(jqxhr, status, error) {
											if (jqxhr.status == 422) {
												var field = Object.keys(jqxhr.responseJSON)[0];
												error = jqxhr.responseJSON[field][0];
											}

											$('.modal-footer').prepend($('<span class="text-danger pull-left"></span>').html(error));
											$('.modal-footer button').removeAttr('disabled');
										}
									});
								});

								return false;
							}
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-default'
						}
					}
				});
			});
		})
		function add_billing_item()
		{
			var str_1 = ''+
			'<form action="/billing-items/add" method="post" class="form-horizontal">'+
				'<div class="form-group">'+
					'<label class="col-md-3 control-label">Plan</label>'+
					'<div class="col-md-9">'+
						'<select name="plan" class="form-control">'+
							'<option value="">Select Plan</option>';


			var str_2 = ''+
				@foreach ($plans as $plan)
							'<option value="{{$plan->id}}">{{$plan->name}}</option>'+
				@endforeach
						'';	

			var str_3 = ''+
						'</select>'+
					'</div>'+
				'</div>'+
				'<div class="form-group">'+
					'<label class="col-md-3 control-label">Start Date</label>'+
					'<div class="col-md-9"><input type="text" name="start_date" class="start-date form-control"></div>'+
				'</div>'+
				'{{ csrf_field() }}'+
			'</form>';

			modalform.dialog({
				bootbox: {
					title: 'Add Billing Item',
					message: str_1.concat(str_2,str_3),
					buttons: {
						cancel: {
							label: 'Cancel',
							className: 'btn-default'
						},
						submit: {
							label: 'Add Billing Item',
							className: 'btn-primary'
						}
					}
				},
				autofocus : false,
				after_init : function() {
					$('.start-date').datetimepicker({
						format : 'DD/MM/YYYY',
						useCurrent : false,
					});
				}
			});
		}
	</script>
@endsection