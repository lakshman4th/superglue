@extends('layouts.default')
@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">
				@if(count($members))
					@foreach($members as $member)
						<div class="col-lg-4">
							<div class="contact-box">
								<a href="">
								<div class="col-sm-4">
									<div class="text-center">
										<img alt="image" class="img-circle m-t-xs img-responsive" src="http://placehold.it/150x150">
										<div class="m-t-xs font-bold">{{ $member->job_title or '' }}</div>
									</div>
								</div>
								</a>
								<div class="col-sm-8">
									<h3><strong>{{ $member->first_name . ' ' . $member->last_name }}</strong></h3>
									<p><i class="fa fa-map-marker"></i> LT2 SubStation</p>
									<address>
										<strong>{{ $member->company_name }}</strong><br>
										Industry: {{ $member->industry }}<br>
										<i class="fa fa-twitter"></i> {!! $member->twitter_handle != '' ? '<a href="https://twitter.com/' . $member->twitter_handle . '">' . $member->twitter_handle . '</a>' : ' ' !!}<br>
									</address>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					@endforeach
				@else
				<p>No Memebers found.</p>
				@endif
			</div>
			<div class="row text-center">
				{!! $members->render() !!}
			</div>
		</div>
@endsection