@extends('layouts.default')
@section('content')
	<div class="wrapper wrapper-content  animated fadeInRight">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox">
						<div class="ibox-title">
							<h5>{{ $title or '' }}</h5>
							<div class="ibox-tools">

							</div>
						</div>
						<div class="ibox-content">
							@if(count($events))
								<table class="table table-striped">
									<thead>
										<tr>
											<th>Title</th>
											<th>Location</th>
											<th>Created At</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
									@foreach($events as $event)
										<tr>
											<td>{{ $event->name }}</td>
											<td>{{ $event->location }}</td>
											<td>{{ $event->created_at }}</td>
											<td>
												<div class="btn-group">
													<button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
													<ul class="dropdown-menu">
														<li><a href="#">View</a></li>
														<li><a href="#">Edit</a></li>
														<li class="divider"></li>
														<li><a href="#">Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							@else
								<div class="text-center">
									<p>No events found in the system</p>
								</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection