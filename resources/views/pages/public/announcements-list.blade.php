@extends('layouts.default')
@section('content')
	<div class="wrapper wrapper-content animated fadeInRight">
			<div class="row">

				<div class="col-lg-6 col-lg-offset-3">
					<div class="ibox">
						<div class="ibox-content text-center">

							<h3 class="m-b-xxs">Announcements</h3>
							<small>What's hip-hop-happinin'</small>

						</div>

					</div>

					@if($announcements->count())
						@foreach($announcements as $announcement)
							<div class="social-feed-box">


								<div class="social-avatar">
									<a href="" class="pull-left">
										<img alt="image" src="/images/a1.jpg">
									</a>
									<div class="media-body">
										<p class="title">
											{{ $announcement->title }}
										</p>
										<small class="text-muted">{{ $announcement->created_at->toDayDateTimeString() . ' (' . $announcement->created_at->diffForHumans() .')' }}</small>
									</div>
								</div>
								<div class="social-body">
									<p>
										{{ $announcement->content }}
									</p>

									<div class="btn-group">
									@if($announcement->link != '')
										<a href="{{ $announcement->link }}"><button class="btn btn-white btn-xs"><i class="fa fa-link"></i> Learn More</button></a>
									@endif
										{{-- <button class="btn btn-white btn-xs"><i class="fa fa-comments"></i> Comment</button>
										<button class="btn btn-white btn-xs"><i class="fa fa-share"></i> Share</button> --}}
									</div>
								</div>
							</div>
						@endforeach
					<div class="text-center">
						{{ $announcements->links() }}
					</div>
					@else
						<p>No Announcements</p>
					@endif
				</div>
			</div>
		</div>
@endsection