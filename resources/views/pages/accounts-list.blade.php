@extends('layouts.default')

@section('content')
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>{{ $title or '' }}</h5>
						<div class="ibox-tools">
							<a href="/admin/accounts/create" class="btn btn-primary btn-xs">Create New Account</a>
						</div>
					</div>
					<div class="ibox-content">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Account Name</th>
									<th>Billing Email</th>
									<th>Created At</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@if ($accounts)
								@foreach ($accounts as $account)
									<tr>
										<td>{{ $account->id }}</td>
										<td>{{ $account->name }}</td>
										<td>{{ $account->email }}</td>
										<td>{{ $account->created_at->setTimezone(Auth::user()->timezone)->format('F jS \a\t g:ia') }}</td>
										<td>
											<div class="btn-group">
													<button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle">Action <span class="caret"></span></button>
													<ul class="dropdown-menu">
														<li><a href="/admin/accounts/view/{{$account->id}}">Manage</a></li>
														<li><a href="/admin/accounts/edit/{{$account->id}}">Edit</a></li>
													</ul>
												</div>
										</td>
									</tr>
								@endforeach
								@else
									<tr>
										<td colspan="6">No accounts</td>
									</tr>
								@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
